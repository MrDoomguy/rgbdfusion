/**
 *  This file is part of dvo.
 *
 *  Copyright 2016 Christian Kerl <christian.kerl@in.tum.de> (Technical University of Munich)
 *  For more information see <http://vision.in.tum.de/data/software/dvo>.
 *
 *  dvo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  dvo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with dvo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DVO_SLAM_INCLUDE_DVO_SLAM_ILLUMINATION_REFINEMENT_HPP_
#define DVO_SLAM_INCLUDE_DVO_SLAM_ILLUMINATION_REFINEMENT_HPP_

#include <dvo/rgbd_image.hpp>

namespace dvo_slam
{

namespace internal
{

class IlluminationRefinementImpl;

}  // namespace internal

class IlluminationRefinement
{
private:
  internal::IlluminationRefinementImpl *impl_;
public:
  IlluminationRefinement();
  ~IlluminationRefinement();

  void refine(const dvo::RgbdImage &img);
};

}  // namespace dvo_slam


#endif /* DVO_SLAM_INCLUDE_DVO_SLAM_ILLUMINATION_REFINEMENT_HPP_ */
