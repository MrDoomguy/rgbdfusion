/**
 *  This file is part of dvo.
 *
 *  Copyright 2016 Christian Kerl <christian.kerl@in.tum.de> (Technical University of Munich)
 *  For more information see <http://vision.in.tum.de/data/software/dvo>.
 *
 *  dvo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  dvo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with dvo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dvo_slam/illumination_refinement.hpp>
#include <dvo/opencv_ext.hpp>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/SparseCholesky>
#include <dvo/timer.hpp>
#include <array>

namespace dvo_slam
{

namespace internal
{

class IlluminationRefinementImpl
{
public:
  dvo::Profiler profiler;
  double fx, fy, ox, oy;
  cv::Mat1f bilateralFilterDepthMap(const cv::Mat1f &depth, const cv::Mat1f &variance)
  {
    dvo::Take t(profiler.time("bilateralFilterDepthMap"));
    cv::Mat1f depth_out(depth.size());

    int hw = 4;

    cv::Mat1f kernel(hw * 2 + 1, hw * 2 + 1);
    kernel.setTo(0);
    for(int yi = -hw; yi <= hw; ++yi)
      for(int xi = -hw; xi <= hw; ++xi)
      {
        kernel(yi + hw, xi + hw) = std::exp(-0.5f * (xi * xi + yi * yi) / (8.0f * 8.0f));
      }
    float factor = -0.5f / (0.05f * 0.05f);
    for(int y = 0; y < depth.rows; ++y)
      for(int x = 0; x < depth.cols; ++x)
      {
        const float &d = depth(y, x);

        if(d != d)// || d > 1.5)
        {
          depth_out(y, x) = std::numeric_limits<float>::quiet_NaN();
        }
        else
        {
          float d_sum = d;
          float w_sum = 1.0f;

          for(int yi = -hw; yi <= hw; ++yi)
            for(int xi = -hw; xi <= hw; ++xi)
            {
              if(xi == 0 && yi == 0) continue;
              int ny = y + yi, nx = x + xi;

              if(nx < 0 || ny < 0 || nx >= depth.cols || ny >= depth.rows) continue;

              const float &nd = depth(ny, nx);
              if(nd == nd)
              {
                float diff = d - nd;
                float w = kernel(yi + hw, xi + hw) * std::exp(factor * (diff * diff));
                d_sum += w * nd;
                w_sum += w;
              }
            }

          depth_out(y, x) = d_sum / w_sum;
        }
      }

    return depth_out;
  }

  void testGraberPerspectiveReparameterization(const dvo::PinholeCamera &cam, const cv::Mat1f &depth)
  {
    cv::Mat1d depth_reparam(depth.size());

    for(int y = 0; y < depth.rows; ++y)
      for(int x = 0; x < depth.cols; ++x)
      {
        depth_reparam(y, x) = depth(y, x) * depth(y, x) * 0.5;
      }

    cv::Mat3d normals(depth.size());
    normals.setTo(cv::Vec3d(0,0,0));

    for(int y = 0; y < depth.rows - 1; ++y)
      for(int x = 0; x < depth.cols - 1; ++x)
      {
        double d = depth_reparam(y, x);
        double d_right = depth_reparam(y, x + 1);
        double d_below = depth_reparam(y + 1, x);

        double dx = d_right - d, dy = d_below - d;

        if(dx != dx || dy != dy) continue;

        double u = (x - cam.ox()) / cam.fx(), v = (y - cam.oy()) / cam.fy();

        cv::Vec3d n;
        n.val[0] = dx / cam.fy();
        n.val[1] = dy / cam.fx();
        n.val[2] = -(u * n.val[0] + v * n.val[1] + 2.0f * d / (cam.fx() * cam.fy()));
        //std::cout << n.val[0] << " "<< n.val[1] << " "<< n.val[2] << std::endl;
        Eigen::Vector3d::Map(n.val).normalize();
        normals(y, x) = n;
      }

    cv::imshow("depth_reparam", depth_reparam);
    cv::imshow("normals_reparam", -normals);
    cv::waitKey(0);
  }

  cv::Mat1f reparamPerspectiveDepth(const cv::Mat1f &depth)
  {
    cv::Mat1f depth_reparam(depth.size());

    for(int y = 0; y < depth.rows; ++y)
      for(int x = 0; x < depth.cols; ++x)
      {
        depth_reparam(y, x) = depth(y, x) * depth(y, x) * 0.5f;
      }

    return depth_reparam;
  }
  cv::Mat1f reverseReparamPerspectiveDepth(const cv::Mat1f &depth)
  {
    cv::Mat1f depth_reparam(depth.size());

    for(int y = 0; y < depth.rows; ++y)
      for(int x = 0; x < depth.cols; ++x)
      {
        depth_reparam(y, x) = std::sqrt(2.0f * depth(y, x));
      }

    return depth_reparam;
  }

  using PixelVector = std::vector<cv::Point>;

  struct PixelIndex
  {
    cv::Mat1i index;
    PixelVector pixels;
  };


  void createPixelIndex(const cv::Mat1f &depth, PixelIndex &index)
  {
    dvo::Take t(profiler.time("createPixelIndex"));
    index.index.create(depth.size());
    index.index.setTo(-1);
    index.pixels.reserve(depth.total());

    for(int y = 0; y < depth.rows - 1; ++y)
      for(int x = 0; x < depth.cols - 1; ++x)
      {
        if(depth(y, x) != depth(y, x)) continue;

        float dx = depth(y, x + 1) - depth(y, x), dy = depth(y + 1, x) - depth(y, x);

        if(dx == dx && dy == dy && std::abs(dx) < 0.05 * 1000.0 && std::abs(dy) < 0.05 * 1000.0)
        {
          index.index(y, x) = index.pixels.size();
          index.pixels.push_back(cv::Point(x, y));
        }
      }
  }

  Eigen::Vector4d estimateSH1Lighting(const PixelIndex &index, const cv::Mat1f &intensity, const cv::Mat1f &depth)
  {
    dvo::Take t(profiler.time("estimateSH1Lighting"));
    Eigen::Matrix4d A; Eigen::Vector4d b;
    A.setZero(); b.setZero();

    Eigen::Vector4d n;
    n(3) = 1.0;

    for(auto &pixel : index.pixels)
    {
      int x = pixel.x, y = pixel.y;
      const float &i = intensity(y, x);

      if(i > 0.95 || i < 0.05) continue;

      double d = depth(y, x);
      double dx = depth(y, x + 1) - d, dy = depth(y + 1, x) - d;
      n.head<3>() = Eigen::Vector3d(dx, dy, -1.0f).normalized().eval();

      A += n * n.transpose();
      b += n * i;
    }

    return A.llt().solve(b);
  }

  cv::Mat1f visualizeSH1Lighting(const PixelIndex &index, const cv::Mat1f &depth, const Eigen::Vector4d &sh)
  {
    dvo::Take t(profiler.time("visualizeSH1Lighting"));
    cv::Mat1f r(depth.size());
    r.setTo(std::numeric_limits<float>::quiet_NaN());
    std::cout << sh.transpose() << std::endl;
    Eigen::Vector4d n;
    n(3) = 1.0;

    double two_over_fxfy = 2.0 / (fx * fy);
    for(auto &pixel : index.pixels)
    {
      int x = pixel.x, y = pixel.y;

      double d = depth(y, x);
      double x_head = (x - ox) / fx, y_head = (y - oy) / fy;
      double dx = depth(y, x + 1) - d, dy = depth(y + 1, x) - d;
#if 0
      dx /= fy;
      dy /= fx;

      n.head<3>() = Eigen::Vector3d(dx, dy, -(x_head * dx + y_head * dy + two_over_fxfy * d)).normalized().eval();
#else
      n.head<3>() = Eigen::Vector3d(dx, dy, -1.0f).normalized().eval();
#endif
      double shr = n.dot(sh);

      //if(shr != shr) throw std::runtime_error("sh is nan in visualizeSH1Lighting()");

      r(y, x) = shr;
    }

    return r;
  }

  static const int Neighbours = 4;
  using WeightVec = cv::Vec<double, Neighbours>;
  using WeightMat = cv::Mat_<WeightVec>;

  template<typename LambdaT>
  void for_each_neighbour(int width, int height, int x, int y, const LambdaT &lambda)
  {
    if(Neighbours == 4)
    {
      dvo::for_each_4neighbours_with_index(width, height, x, y, lambda);
    }
    if(Neighbours == 8)
    {
      dvo::for_each_8neighbours_with_index(width, height, x, y, lambda);
    }
  }

  WeightMat computeWeights(const cv::Mat1f &intensity, const cv::Mat1f &depth)
  {
    dvo::Take t(profiler.time("computeWeights"));
    WeightMat weights(intensity.size());
    double inv_two_sigma_d_sqr = 1.0 / (2.0 * 50), inv_two_sigma_c_sqr = 1.0 / (2.0 * 0.1), tau = 0.3;

    // precompute weights
    for(int y = 0; y < intensity.rows; ++y)
      for(int x = 0; x < intensity.cols; ++x)
      {
        WeightVec &w = weights(y, x);
        std::fill(w.val, w.val + Neighbours, 0.0);

        const float &i = intensity(y, x);
        const float &d = depth(y, x);

        if(d != d) continue;

        for_each_neighbour(intensity.cols, intensity.rows, x, y, [&](int k, int xi, int yi) {
          const float &i_k = intensity(yi, xi);
          const float &d_k = depth(yi, xi);

          float delta_i = i - i_k;
          delta_i *= delta_i;

          if(delta_i > tau || d_k != d_k)
          {
            w[k] = 0.0;
          }
          else
          {
            float delta_d = d - d_k;
            delta_d *= delta_d;
            w[k] = std::exp(-delta_d * inv_two_sigma_d_sqr -delta_i * inv_two_sigma_c_sqr);
          }
        });
      }

    return weights;
  }

  cv::Mat1f estimateAlbedo(const PixelIndex &index, const cv::Mat1f &intensity, const WeightMat &weights, cv::Mat1f &sh_lighting)
  {
    std::cout << "estimating rho" << std::endl;
    cv::Mat1f rho_cholesky = estimateAlbedoCholesky(index, intensity, weights, sh_lighting);

    cv::imshow("rho", cv::abs(rho_cholesky) * 0.5);
    return rho_cholesky;
  }
  cv::Mat1f estimateAlbedoCholesky(const PixelIndex &index, const cv::Mat1f &intensity, const WeightMat &weights, cv::Mat1f &sh_lighting)
  {
    dvo::Take t(profiler.time("estimateAlbedoCholesky"));
    float lambda_rho = 1.0;

    int n_pixels = index.pixels.size();

    using MatrixA = Eigen::SparseMatrix<float>;
    MatrixA A(n_pixels, n_pixels);

    Eigen::VectorXf b(n_pixels), DtD(n_pixels);

    using Triplet = Eigen::Triplet<float>;
    using TripletVector = std::vector<Triplet>;

    TripletVector A_triplets;
    A_triplets.reserve(intensity.total() * (1 + (Neighbours)));

    int idx = 0;
    for(auto &pixel : index.pixels)
    {
      int y = pixel.y, x = pixel.x;
      const WeightVec &w = weights(y, x);
      const float &sh = sh_lighting(y, x);
      const float &i = intensity(y, x);

      float w_sum = 0.0;

      for_each_neighbour(intensity.cols, intensity.rows, x, y, [&](int k, int xi, int yi) {
        int idx_k = index.index(yi, xi);
        if(idx_k < 0) return;
        A_triplets.push_back(Triplet(idx, idx_k, -w[k]));
        w_sum += w[k];
      });
      A_triplets.push_back(Triplet(idx, idx, w_sum));

      //if(sh != sh) throw std::runtime_error("sh is nan!");

      DtD(idx) = sh * sh;
      b(idx) = sh * i;

      ++idx;
    }

    A.setFromTriplets(A_triplets.begin(), A_triplets.end());

    MatrixA AtA_custom = A.transpose() * A * lambda_rho;

    for(int idx = 0; idx < n_pixels; ++idx)
      AtA_custom.coeffRef(idx, idx) += DtD(idx);

    Eigen::ConjugateGradient<MatrixA> solver;
    {
      dvo::Take t(profiler.time("solver.analyzePattern(AtA)"));
      solver.analyzePattern(AtA_custom);
    }
    {
      dvo::Take t(profiler.time("solver.factorize(AtA)"));
      solver.factorize(AtA_custom);
    }
    //solver.compute(AtA_custom);

    if(solver.info() != Eigen::Success) throw std::runtime_error("decomposition of A failed!");

    Eigen::VectorXf x = solver.solve(b);

    if(solver.info() != Eigen::Success) throw std::runtime_error("solving b failed!");

    cv::Mat1f rho(intensity.size());
    rho.setTo(0);

    idx = 0;
    for(auto &pixel : index.pixels)
    {
      rho(pixel.y, pixel.x) = x(idx);
      ++idx;
    }
    return rho;
  }

  cv::Mat1f estimateLightingVariation(const PixelIndex &index, const cv::Mat1f &intensity_minus_sh_times_rho, const WeightMat &weights)
  {
    std::cout << "estimating beta" << std::endl;
    auto beta = estimateLightingVariationCholesky(index, intensity_minus_sh_times_rho, weights);
    cv::imshow("beta", beta);
    return beta;
  }
  cv::Mat1f estimateLightingVariationCholesky(const PixelIndex &index, const cv::Mat1f &intensity_minus_sh_times_rho, const WeightMat &weights)
  {
    double lambda_beta_1 = 1.0, lambda_beta_2 = 1.0;

    int n_pixels = index.pixels.size();

    using MatrixA = Eigen::SparseMatrix<float>;
    MatrixA  A(n_pixels, n_pixels);

    Eigen::VectorXf b(n_pixels);

    using Triplet = Eigen::Triplet<float>;
    using TripletVector = std::vector<Triplet>;

    TripletVector A_triplets;
    A_triplets.reserve(n_pixels * (1 + (Neighbours)));

    int idx = 0;
    for(auto &pixel : index.pixels)
    {
      int y = pixel.y, x = pixel.x;
      const WeightVec &w = weights(y, x);
      const float &i = intensity_minus_sh_times_rho(y, x);

      float w_sum = 0.0;

      for_each_neighbour(intensity_minus_sh_times_rho.cols, intensity_minus_sh_times_rho.rows, x, y, [&](int k, int xi, int yi) {
        int idx_k = index.index(yi, xi);
        if(idx_k < 0) return;

        A_triplets.push_back(Triplet(idx, idx_k, -w[k]));
        w_sum += w[k];
      });
      A_triplets.push_back(Triplet(idx, idx, w_sum));

      b(idx) = i;
      ++idx;
    }

    A.setFromTriplets(A_triplets.begin(), A_triplets.end());

    MatrixA AtA_custom = A.transpose() * A * lambda_beta_1;

    for(int idx = 0; idx < n_pixels; ++idx)
      AtA_custom.coeffRef(idx, idx) += 1.0 + lambda_beta_2 * 1.0;

    Eigen::ConjugateGradient<MatrixA> solver;
    solver.compute(AtA_custom);

    if(solver.info() != Eigen::Success) throw std::runtime_error("decomposition of A failed!");

    Eigen::VectorXf x = solver.solve(b);

    if(solver.info() != Eigen::Success) throw std::runtime_error("solving b failed!");

    cv::Mat1f beta(intensity_minus_sh_times_rho.size());
    beta.setTo(0);

    idx = 0;
    for(auto &pixel : index.pixels)
    {
      beta(pixel.y, pixel.x) = x(idx);
      ++idx;
    }

    return beta;
  }

  cv::Mat1f laplacianSmoothingCholesky(const cv::Mat1f &intensity, double lambda_beta_1)
  {
      int n_pixels = intensity.total();

      using MatrixA = Eigen::SparseMatrix<float>;
      MatrixA  A(n_pixels, n_pixels);

      Eigen::VectorXf b(n_pixels);

      using Triplet = Eigen::Triplet<float>;
      using TripletVector = std::vector<Triplet>;

      TripletVector A_triplets;
      A_triplets.reserve(n_pixels * (1 + (Neighbours)));

      int idx = 0;

      for(int y = 0; y < intensity.rows; ++y)
        for(int x = 0; x < intensity.cols; ++x, ++idx)
        {
          const float &i = intensity(y, x);

          if(y > 0 && x > 0 && y < intensity.rows - 1 && x < intensity.cols - 1)
          {
            int neighbours = 0;
            for_each_neighbour(intensity.cols, intensity.rows, x, y, [&intensity, &A_triplets, &idx, &neighbours](int k, int xi, int yi) {
              int idx_k = yi * intensity.cols + xi;
              if(idx_k < 0) return;

              A_triplets.push_back(Triplet(idx, idx_k, -1));
              neighbours += 1;
            });
            A_triplets.push_back(Triplet(idx, idx, neighbours));
          }
          b(idx) = i;
        }

      A.setFromTriplets(A_triplets.begin(), A_triplets.end());

      MatrixA AtA_custom = A.transpose() * A * lambda_beta_1;

      for(int idx = 0; idx < n_pixels; ++idx)
        AtA_custom.coeffRef(idx, idx) += 1.0;

      Eigen::ConjugateGradient<MatrixA> solver;
      solver.compute(AtA_custom);

      if(solver.info() != Eigen::Success) throw std::runtime_error("decomposition of A failed!");

      Eigen::VectorXf x = solver.solve(b);

      if(solver.info() != Eigen::Success) throw std::runtime_error("solving b failed!");

      cv::Mat1f beta(intensity.size());
      Eigen::VectorXf::Map(beta.ptr<float>(), beta.total()) = x.cast<float>();

      return beta;
  }

  struct PixelContext
  {
    int x, y, width, height, index;
  };

  using Tripletf = Eigen::Triplet<float>;
  using TripletfVector = std::vector<Tripletf>;

  void makeLaplacianJ(const PixelContext &ctx, TripletfVector &triplets)
  {
    int neighbours = 0;
    for_each_neighbour(ctx.width, ctx.height, ctx.x, ctx.y, [&ctx, &triplets, &neighbours](int k, int xi, int yi) {
      int idx_k = yi * ctx.width + xi;
      if(idx_k < 0) return;

      triplets.push_back(Tripletf(ctx.index, idx_k, -1.0f));
      neighbours += 1;
    });
    triplets.push_back(Tripletf(ctx.index, ctx.index, neighbours));
  }

  Tripletf sortRowColumn(const Tripletf &triplet)
  {
    if(triplet.row() <= triplet.col())
    {
      return triplet;
    }
    else
    {
      return Tripletf(triplet.col(), triplet.row(), triplet.value());
    }
  }
  void makeLaplacianJtJ(const PixelContext &ctx, TripletfVector &triplets)
  {
    int num_neighbours = 0;
    struct NeighbourInfo
    {
      int index;
      float weight;
    };
    std::array<NeighbourInfo, Neighbours> neighbours;
    for_each_neighbour(ctx.width, ctx.height, ctx.x, ctx.y, [&ctx, &neighbours,&num_neighbours] (int k, int xi, int yi) {
      int idx_k = yi * ctx.width + xi;

      neighbours[num_neighbours]= { idx_k, -1.0f };

      num_neighbours += 1;
    });

    for(int i = 0; i < num_neighbours; ++i)
    {
      for(int j = i + 1; j < num_neighbours; ++j)
      {
        triplets.push_back(sortRowColumn(Tripletf(neighbours[i].index, neighbours[j].index, neighbours[i].weight * neighbours[j].weight)));
        //triplets.push_back(Tripletf(neighbours[j].index, neighbours[i].index, neighbours[i].weight * neighbours[j].weight));
      }
      triplets.push_back(Tripletf(neighbours[i].index, neighbours[i].index, neighbours[i].weight * neighbours[i].weight));
      triplets.push_back(sortRowColumn(Tripletf(ctx.index, neighbours[i].index, neighbours[i].weight * num_neighbours)));
      //triplets.push_back(Tripletf(neighbours[i].index, ctx.index, neighbours[i].weight * num_neighbours));
    }

    triplets.push_back(Tripletf(ctx.index, ctx.index, num_neighbours * num_neighbours));
  }

  void testLaplacianJtJ(cv::Size size)
  {
    TripletfVector triplets_J, triplets_JtJ;
    int n_pixels = size.area();
    Eigen::SparseMatrix<float> J(n_pixels, n_pixels), JtJ_direct(n_pixels, n_pixels), JtJ_multiply(n_pixels, n_pixels);

//    int x = 1, y = 1;
    PixelContext ctx = {0, 0, size.width, size.height, 0};

    for(ctx.y = 0; ctx.y < size.height; ++ctx.y)
      for(ctx.x = 0; ctx.x < size.width; ++ctx.x, ++ctx.index)
      {
        makeLaplacianJ(ctx, triplets_J);
        makeLaplacianJtJ(ctx, triplets_JtJ);
      }

    J.setFromTriplets(triplets_J.begin(), triplets_J.end());
    JtJ_direct.setFromTriplets(triplets_JtJ.begin(), triplets_JtJ.end());

    JtJ_multiply = J.transpose() * J;
    Eigen::MatrixXf kernel(size.width, size.height);

    int idx = 0;
    for(int y = 0; y < size.height; ++y)
      for(int x = 0; x < size.width; ++x, ++idx)
      {
        int r = size.height / 2 * size.width + size.width / 2;
        kernel(y, x) = JtJ_multiply.coeff(r, idx);
      }



    std::cout << JtJ_direct << std::endl;
    std::cout << JtJ_multiply << std::endl;
    std::cout << JtJ_multiply - JtJ_direct << std::endl;
    std::cout << kernel << std::endl;
  }

  void testLaplacianSmooting(cv::Mat1f &intensity)
  {
    int last_weight = 0, weight_int = 0, weight_not_changed = 0;
    cv::namedWindow("laplacian_smoothing");
    cv::createTrackbar("weight", "laplacian_smoothing", &weight_int, 1000);

    testLaplacianJtJ(cv::Size(5,5));
    throw std::runtime_error("abort");

    cv::Mat1f result = intensity.clone(), result2 = intensity.clone();
    while(true)
    {
      if(last_weight != weight_int)
      {
        last_weight = weight_int;
        weight_not_changed = 0;
      }
      else
      {
        weight_not_changed += 1;
      }

      if(weight_not_changed == 200)
      {
        std::cout << "calculating" << std::endl;
        result = laplacianSmoothingCholesky(intensity, weight_int * 0.1);
      }

      cv::imshow("laplacian_smoothing", result);
      cv::waitKey(10);
    }
  }

  cv::Mat1f estimateDepth(const PixelIndex &index, const cv::Mat1f &depth_in, const Eigen::Vector4d &sh, const cv::Mat1f &rho, const cv::Mat1f &intensity_minus_beta)
  {
    float lambda_z_1 = 0.00004, lambda_z_2 = 0.00075 * 2.0;
    int n_pixels = index.pixels.size();

    using MatrixA = Eigen::SparseMatrix<float>;
    MatrixA J_dataterm(n_pixels, n_pixels), J_laplacian(n_pixels, n_pixels);

    cv::Mat3f normals(depth_in.size());
    normals.setTo(cv::Vec3f(0,0,0));
    cv::Mat1f err_dataterm(depth_in.size()), err_closeness(depth_in.size()), err_laplacian(depth_in.size()), reconstruction(depth_in.size());
    err_dataterm.setTo(0);
    err_closeness.setTo(0);
    err_laplacian.setTo(0);
    reconstruction.setTo(0);

    using Triplet = Eigen::Triplet<float>;
    using TripletVector = std::vector<Triplet>;
    TripletVector J_laplacian_triplets, J_dataterm_triplets;
    J_laplacian_triplets.reserve(J_laplacian.rows() * (1 + Neighbours));

    Eigen::VectorXf r_dataterm(n_pixels), b_closeness(n_pixels), z_last(n_pixels);

    int idx = 0;
    for(auto &pixel : index.pixels)
    {
      b_closeness(idx) = depth_in(pixel.y, pixel.x);
      z_last(idx) = depth_in(pixel.y, pixel.x);
      ++idx;
    }

    double last_chi2 = 1e30;
    float two_over_fxfy = 2.0 / (fx * fy), one_over_fx = 1.0f / fx, one_over_fy = 1.0f /fy;

    for(int it = 0; it < 10; ++it)
    {
      J_dataterm_triplets.clear();
      J_laplacian_triplets.clear();
      double chi2 = 0.0, chi2_data = 0.0, chi2_closeness = 0.0, chi2_smoothness = 0.0;
      int idx = 0;
      for(auto &pixel : index.pixels)
      {
        int y = pixel.y, x = pixel.x;
        const float &i = intensity_minus_beta(y, x);
        const float &rho_ = rho(y, x);
        int idx_right = index.index(y, x + 1), idx_below = index.index(y + 1, x);

        // build dataterm Jacobian and residual
        bool not_boundary = idx_right >= 0 && idx_below >= 0;
        if(true)
        {

          float d = z_last(idx);
          float x_head = (x - ox) / fx, y_head = (y - oy) / fy;
          float z_dx, z_dy;
          if(not_boundary)
          {
            z_dx = z_last(idx_right) - d, z_dy = z_last(idx_below) - d;
          }
          else
          {
            z_dx = 0;
            z_dy = 0;
          }
#if 0
          z_dx *= one_over_fx;
          z_dy *= one_over_fy;

          float z_dz = -(x_head * z_dx + y_head * z_dy + two_over_fxfy * d);
#else
          float z_dz = -1.0f;
#endif
          float weight = 1.0f / std::sqrt(z_dz* z_dz + z_dx * z_dx + z_dy * z_dy);
          Eigen::Vector3f::Map(normals(y,x).val) = Eigen::Vector3f(float(z_dx), float(z_dy), float(z_dz)) * weight;
          //for(int ni = 0; ni < 3; ++ni) normals(y,x).val[ni] = std::abs(tmp(ni));

          weight *= rho_;
          if(not_boundary)
          {
#if 0
            float J_z = weight * (sh(0) * -one_over_fy + sh(1) * -one_over_fx + sh(2) * (x_head * one_over_fy + y_head * one_over_fx - two_over_fxfy)),
              J_z_right = weight * (sh(0) * one_over_fy + sh(2) * -x_head * one_over_fy),
              J_z_below = weight * (sh(1) * one_over_fx + sh(2) * -y_head * one_over_fx);
#else
            float J_z = weight * (sh(0) * -1.0f + sh(1) * -1.0f), J_z_right = weight * sh(0), J_z_below = weight * sh(1);
#endif
            J_dataterm_triplets.push_back(Triplet(idx, idx, J_z));

            J_dataterm_triplets.push_back(Triplet(idx, idx_right, J_z_right));
            J_dataterm_triplets.push_back(Triplet(idx, idx_below, J_z_below));
          }
          else
          {
#if 0
            float J_z = weight * (sh(2) * (- two_over_fxfy));
            J_dataterm_triplets.push_back(Triplet(idx, idx, J_z));
#endif
          }
#if 0
          r_dataterm(idx) = i - rho_ * sh(3);
#else
          r_dataterm(idx) = i - weight * sh(2) * z_dz - rho_ * sh(3);
#endif

          float r = weight * (z_dx * sh(0) + z_dy * sh(1) + sh(2) * z_dz) + rho_ * sh(3);
          reconstruction(y, x) = r;
          r -= i;
          chi2_data += r * r;
          err_dataterm(y, x) = std::abs(r);
        }

        float r_laplacian = 0.0;
        // build laplacian Jacobian
        int n_neighbours = 0;
        for_each_neighbour(depth_in.cols, depth_in.rows, x, y, [&](int k, int xi, int yi) {
          int idx_k = index.index(yi, xi);
          if(idx_k < 0) return;

          J_laplacian_triplets.push_back(Triplet(idx, idx_k, -1));
          r_laplacian -= z_last(idx_k);
          n_neighbours += 1;
        });
        r_laplacian += n_neighbours * z_last(idx);
        J_laplacian_triplets.push_back(Triplet(idx, idx, n_neighbours));

        err_laplacian(y, x) = std::abs(r_laplacian);
        chi2_smoothness += lambda_z_2 * r_laplacian * r_laplacian;

        ++idx;
      }

      J_dataterm.setFromTriplets(J_dataterm_triplets.begin(), J_dataterm_triplets.end());
      J_laplacian.setFromTriplets(J_laplacian_triplets.begin(), J_laplacian_triplets.end());

      MatrixA AtA = J_dataterm.transpose() * J_dataterm + lambda_z_2 * J_laplacian.transpose() * J_laplacian;
      Eigen::VectorXf b = J_dataterm.transpose() * r_dataterm;

      // add closeness term
      for(int idx = 0; idx < n_pixels; ++idx)
      {
        const float &d = b_closeness(idx);
        AtA.coeffRef(idx, idx) += lambda_z_1 * 1.0;
        b(idx) += lambda_z_1 * d;

        if(d != d) throw std::runtime_error("nan in closeness");

        float r = z_last(idx) - d;
        chi2_closeness += lambda_z_1 * r * r;
        auto &px = index.pixels.at(idx);

        err_closeness(px.y, px.x) = r;
      }
      chi2 = chi2_data + chi2_smoothness + chi2_closeness;
      std::cout << "chi2_data: " << chi2_data << " chi2_smoothness: " << chi2_smoothness << " chi2_closeness: " << chi2_closeness << " chi2: " << chi2 << std::endl;

      if(chi2 >= last_chi2) break;
      last_chi2 = chi2;

      cv::Mat mask = depth_in == depth_in;
      cv::imshow("normals", -normals);
      cv::imshow("reconstruction", reconstruction);
      //cv::imshow("err_dataterm", dvo::remapToHsvRangeNoNormalize(err_dataterm * 10.0, 120.0f, 0.0f, mask));
      //cv::imshow("err_closeness", dvo::remapToHsvRangeNoNormalize(err_closeness * 10.0, 120.0f, 0.0f, mask));
      //cv::imshow("err_laplacian", dvo::remapToHsvRangeNoNormalize(err_laplacian * 10.0, 120.0f, 0.0f, mask));
      cv::waitKey(5);
      Eigen::ConjugateGradient<MatrixA> solver;
      solver.compute(AtA);

      if(solver.info() != Eigen::Success) throw std::runtime_error("decomposition of A failed!");

      z_last = solver.solve(b);

      if(solver.info() != Eigen::Success) throw std::runtime_error("solving b failed!");

    }

    cv::Mat1f depth_out(depth_in.size());
    depth_out.setTo(std::numeric_limits<float>::quiet_NaN());
    idx = 0;
    for(auto &pixel : index.pixels)
    {
      depth_out(pixel.y, pixel.x) = z_last(idx);
      ++idx;
    }

    return depth_out;
  }
};

}  // namespace internal

IlluminationRefinement::IlluminationRefinement() : impl_(new internal::IlluminationRefinementImpl) {}

IlluminationRefinement::~IlluminationRefinement() { delete impl_; }

void IlluminationRefinement::refine(const dvo::RgbdImage &img)
{
  impl_->fx = img.depthCamera().fx();
  impl_->fy = img.depthCamera().fy();
  impl_->ox = img.depthCamera().ox();
  impl_->oy = img.depthCamera().oy();
  cv::Mat1f intensity = img.intensity<float>();
  cv::Mat1f bilateral_depth = impl_->bilateralFilterDepthMap(img.depth<float>(), img.depthVariance<float>());
#if 0
  cv::Mat1f depth = impl_->reparamPerspectiveDepth(bilateral_depth);
#else
  cv::Mat1f depth = bilateral_depth  * 1000.0;
#endif
  dvo::RgbdImage tmp(img.camera());
  tmp.setDepth(bilateral_depth);

  //impl_->testLaplacianSmooting(intensity);

  //impl_->testGraberPerspectiveReparameterization(img.depthCamera(), depth);

  for(int it = 0; it < 1; ++it)
  {
    internal::IlluminationRefinementImpl::PixelIndex index;
    impl_->createPixelIndex(depth, index);
    cv::Mat3f normals = tmp.normals<float>();
    Eigen::Vector4d sh = impl_->estimateSH1Lighting(index, intensity, depth);

    cv::Mat1f sh_lighting = impl_->visualizeSH1Lighting(index, depth, sh);

    cv::imshow("normals_in", -normals);
    cv::imshow("lighting", sh_lighting);
    cv::imshow("intensity", intensity);
    //cv::imshow("normalized", intensity - sh_lighting);
    cv::waitKey(0);
    auto weights = impl_->computeWeights(intensity, depth);

    cv::Mat1f rho = impl_->estimateAlbedo(index, intensity, weights, sh_lighting);
    cv::Mat1f sh_rho = sh_lighting.mul(rho);
    cv::Mat1f intensity_minus_sh_rho = intensity - sh_rho;
    //cv::imshow("sh+rho", sh_rho);
    //cv::imshow("normalized2", intensity_minus_sh_rho);
    cv::waitKey(0);

    cv::Mat1f beta = impl_->estimateLightingVariation(index, intensity_minus_sh_rho, weights);
    //cv::waitKey(0);
    cv::Mat1f intensity_minus_beta = intensity - beta;

#if 0
    cv::Mat1f depth_refined = impl_->reverseReparamPerspectiveDepth(impl_->estimateDepth(index, depth, sh, rho, intensity_minus_beta));
#else
    cv::Mat1f depth_refined = impl_->estimateDepth(index, depth, sh, rho, intensity_minus_beta);
#endif
    std::cout << impl_->profiler << std::endl;

    tmp.setDepth(depth_refined);
    cv::Mat3f normals_upscaled;
    normals = tmp.normals<float>();
    //cv::resize(normals, normals_upscaled, cv::Size(), 2, 2);
    cv::imshow("normals_opt", -normals);
    cv::waitKey(0);
    depth = depth_refined;
  }
}


}  // namespace dvo_slam
