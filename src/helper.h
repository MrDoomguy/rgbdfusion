// ###
// ###
// ### Practical Course: GPU Programming in Computer Vision
// ###
// ###
// ### Technical University Munich, Computer Vision Group
// ### Winter Semester 2015/2016, March 15 - April 15
// ###
// ###
// ###
// ### 
// ###
// ###


#ifndef HELPER_H
#define HELPER_H

#include <cuda_runtime.h>
#include <ctime>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <sstream>
#include <cstdlib>
#include <iostream>
#include "cublas_v2.h"
#include "cusolverDn.h"


#define SAFE_RELEASE_ARRAY(p) if(p) delete[] p;

struct PixelCoord
{
    PixelCoord() {
        x = 0;
        y = 0;
        pos = 0;
    }
    PixelCoord(uint32_t _x, uint32_t _y, uint32_t _pos) {
        x = _x;
        y = _y;
        pos = _pos;
    }
    uint32_t x, y, pos;
};

struct RightAndLowerWeights {
    __host__ __device__ RightAndLowerWeights() {
        right = 0;
        lower = 0;
    }
    __host__ __device__ RightAndLowerWeights(float _right, float _lower) {
        right = _right;
        lower = _lower;
    }
    float right, lower;
};

// parameter processing
template<typename T>
bool getParam(std::string param, T &var, int argc, char **argv)
{
    const char *c_param = param.c_str();
    for(int i=argc-1; i>=1; i--)
    {
        if (argv[i][0]!='-') continue;
        if (strcmp(argv[i]+1, c_param)==0)
        {
            if (!(i+1<argc)) continue;
            std::stringstream ss;
            ss << argv[i+1];
            ss >> var;
            return (bool)ss;
        }
    }
    return false;
}




// opencv helpers
void showImage(std::string title, const cv::Mat &mat, int x, int y);
void showHistogram256(const char *windowTitle, int *histogram, int windowX, int windowY);



// adding Gaussian noise
void addNoise(cv::Mat &m, float sigma);




// measuring time
class Timer
{
    public:
	Timer() : tStart(0), running(false), sec(0.f)
	{
	}
	void start()
	{
		tStart = clock();
		running = true;
	}
	void end()
	{
		if (!running) { sec = 0; return; }
        cudaDeviceSynchronize();
		clock_t tEnd = clock();
		sec = (float)(tEnd - tStart) / CLOCKS_PER_SEC;
		running = false;
	}
	float get()
	{
		if (running) end();
		return sec;
	}
    private:
	clock_t tStart;
	bool running;
	float sec;
};




// cuda error checking
#define CUDA_CHECK cuda_check(__FILE__,__LINE__)
void cuda_check(std::string file, int line);

// cublas error checking
void CUBLAS_CHECK(cublasStatus_t error);

void CUSOLVER_CHECK(cusolverStatus_t error);



#endif  // AUX_H
