% Template created by Robert Maier, 2013
\documentclass[t,plaincaption]{beamer}

\mode<presentation>
{
	\usepackage{theme_cvpr/beamerthemeCVPR}
	\setbeamercovered{transparent}
}

\usepackage{verbatim}

% Use xelatex to use TTF fonts 
\usepackage{fontspec}
\setsansfont{Arial}

% set the bibliography style
\bibliographystyle{abbrv}
%\bibliographystyle{apalike}

% set document information
\def\titleEn{Shading-Based Refinement of Depth Images with CUDA}
\def\authorName{Michael Gutbrod, Lorenzo La Spina, Andreas Seibold}
\title[\titleEn]{\titleEn}
\author[\titleEn]{\authorName}
\date{April 14, 2016}


\begin{document}

\frame{
\titlepage
}

\frame{
\frametitle{Outline}

\tableofcontents
}


\section{Introduction and Motivation}
\frame{
\frametitle{Overview}

  We want to enhance the depth map by fusing the intensity and depth information to create more detailed range profiles.\cite{or2015rgbd}
\begin{itemize}
    \item Bilateral Filter
    \item Shading Computation
    \item Albedo Recovery
    \item Enhance the surface
\end{itemize}
}


\section{Algorithm}
\frame{
\frametitle{Bilateral Filter}
\begin{itemize}
\item Depth input is noisy due to measurement inaccuracies
\item Apply bilateral filtering to obtain a smoothed estimate
\end{itemize}

\begin{columns}

\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/normals.png}
	\caption{Depth Input}
\end{figure}

\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/normals_smoothed.png}
	\caption{Bilateral Filtered Depth}
\end{figure}
\end{columns}
}


%\section{Shading Computation}
\frame{
\frametitle{Shading Computation}

\begin{itemize}
  \item Spherical harmonics account for 87.5\% of scene lighting \cite{basri2003lambertian}
  \item four values calculated from whole bilateral filtered depth image
  \item $ S(\vec n) = \vec m^{T} \tilde n $
\end{itemize}

\begin{columns}

\column[T]{0.7\linewidth}

\begin{figure}
	\includegraphics[width=0.7\textwidth]{img/shading.png}
\end{figure}
\end{columns}

}

 
%\section{Albedo Recovery}
\frame{
\frametitle{Albedo Recovery}

\begin{itemize}
  \item $\rho $ accounts for multiple scene albedos and shadowed areas
	\item assumes same albedos for approximately same depths and only slight changes in intensity
	\item $\lambda$ indicates the strength of the correction factors
\end{itemize}

\begin{columns}

\column[t]{0.7\linewidth}
\begin{figure}
	\includegraphics[width=0.6\textwidth]{img/rholambda0.png}
	\caption{$\rho$ with $\lambda=0$}
\end{figure}

\end{columns}
}


\frame{
\frametitle{Albedo Recovery}

\begin{columns}
	\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/rholambda25_badWeights.png}
	\caption{$\rho$ with $\lambda=25$ and simple weights}
\end{figure}

\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/rholambda25_goodWeights.png}
	\caption{$\rho$ with $\lambda=25$ and complex weights}
\end{figure}
\end{columns}
\vspace{0.6cm}

usually $\lambda \in [0.1;1.0]$

}


%\section{Enhance the surface}
\frame{
\frametitle{Surface Refinement}

\begin{itemize}
  \item Adjust/refine normals
	\item penalize changes in depth 
	\item smooth the depth image 
\end{itemize}


}

\frame{
\frametitle{Surface Refinement}
	\begin{columns}
	\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/shading.png}
	\caption{Shading after first iteration}
\end{figure}

\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/shadingSlider.png}
	\caption{Shading after three iterations}
\end{figure}
\end{columns}
}

\frame{
\frametitle{Surface Refinement}
\begin{columns}
	\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/out_bilateral.png}
	\caption{Depth after first iteration}
\end{figure}

\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/refinedDepthSlider.png}
	\caption{Depth after three iterations}
\end{figure}
\end{columns}
}

\frame{
\frametitle{Surface Refinement}
\begin{columns}
	\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/normals_smoothed.png}
	\caption{Normals after first iteration}
\end{figure}

\column[t]{0.48\linewidth}
\begin{figure}
	\includegraphics[width=1\textwidth]{img/normalsSlider.png}
	\caption{Normals after three iterations}
\end{figure}
\end{columns}
}

\section{Live Demo}
\frame{
\frametitle{Live Demo}

}

\section{Q\&A}
\frame{
\frametitle{Q\&A}
	\centering
	\vspace{2.4cm}
	Questions?

}


\frame[allowframebreaks]{
\frametitle{Bibliography}
	\tiny
	\bibliography{bibliography} 
}

\end{document}
