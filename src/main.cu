#include <opencv2/opencv.hpp>
#include <iostream>
#include <cstdint>
#include <stdio.h>

#include "GPUilluminationRefinement.cu"

using namespace std;

// globals
GPUilluminationRefinement* gpuRefine;
int32_t lambdaRhoSlider = 1, lambdaZ1Slider = 400, lambdaZ2Slider = 400, iterationSlider = 5, algIterationSlider = 1;
ImageGS* rho;
ImageGS* refinedDepth;
ImageGS* shading;
ImageRGB* normals;

// do whole algorithm here
void ExecuteAlgorithm() {
    for(int32_t it = 0; it < algIterationSlider; it++) 
        gpuRefine->multipleAlbedoRecovery(lambdaRhoSlider / 10.f, lambdaZ1Slider / 10000.f, lambdaZ2Slider / 10000.f, iterationSlider, it);
    gpuRefine->downloadRho(*rho);
    gpuRefine->downloadShading(*shading);
    gpuRefine->downloadRefinedDepth(*refinedDepth);
    gpuRefine->downloadNormals(*normals);
    rho->Show("rhoTrack", 0.5, 100, 100);
    refinedDepth->Show("refinedDepthTrack", 1.f/5000.f, 100, 100+480);
    normals->Show("normals refined", -1.0f ,100+640, 100+480);
    shading->Show("S", 100+640, 100);
    cv::imwrite("rhoSlider.png", ((*(*rho).GetMatrixCV()))*(255.f));
    cv::imwrite("refinedDepthSlider.png", ((*(*refinedDepth).GetMatrixCV()))*(255.f/5000.f));
    cv::imwrite("normalsSlider.png", ((*(*normals).GetMatrixCV()))*(-255.f));
    cv::imwrite("shadingSlider.png", ((*(*shading).GetMatrixCV()))*(255.f));
}

// trackbar callback
void onTrackbarLambdaRho(int, void*) {
    Timer timer; timer.start();
    ExecuteAlgorithm();
    timer.end(); float t = timer.get();  // elapsed time in seconds
    cout << "time lambda rho slider: " << t*1000 << " ms" << endl;
}

void onTrackbarLambdaZ1(int, void*) {
    Timer timer; timer.start();
    ExecuteAlgorithm();
    timer.end(); float t = timer.get();  // elapsed time in seconds
    cout << "time lambda Z1 slider: " << t*1000 << " ms" << endl;
}

void onTrackbarLambdaZ2(int, void*) {
    Timer timer; timer.start();
    ExecuteAlgorithm();
    timer.end(); float t = timer.get();  // elapsed time in seconds
    cout << "time lambda Z2 slider: " << t*1000 << " ms" << endl;
}

void onTrackbarIterations(int, void*) {
    Timer timer; timer.start();
    ExecuteAlgorithm();
    timer.end(); float t = timer.get();  // elapsed time in seconds
    cout << "time lambda iterations slider: " << t*1000 << " ms" << endl;
}


//CPU stuff
float* cpuComputeGaussianKernel(uint32_t* hw, int32_t* radius) {
    float sigma = 8.f;
    
    *radius = 4;
    int32_t rad = *radius;
    *hw = 2*rad+1;
    
    float* kernel = new float[(2*rad+1) * (2*rad+1)];
    for(int32_t x = -rad; x <= rad; x++) {
        for(int32_t y = -rad; y <= rad; y++) {
            size_t idx = x+rad + (size_t)(*hw)*(y+rad);
            kernel[idx] = expf(-((x*x + y*y)/(2.f * sigma * sigma)));
        }
    }
    
    return kernel;
}

uint32_t countValidPixels(const cv::Mat1f &depth, vector<PixelCoord> &PixelCoords) {
    uint32_t result = 0;
    for(int y = 0; y < depth.rows - 1; ++y) {
        for(int x = 0; x < depth.cols - 1; ++x) {
            if(depth(y, x) != depth(y, x)) continue;

            float dx = depth(y, x + 1) - depth(y, x), dy = depth(y + 1, x) - depth(y, x);

            if(dx == dx && dy == dy && std::abs(dx) < 0.05 * 1000.0 && std::abs(dy) < 0.05 * 1000.0) {
                PixelCoords.push_back(PixelCoord(x, y, result));
                result++;
            }
        }
    }
    return result;
}






int main(int argc, char **argv)
{

    // Before the GPU can process your kernels, a so called "CUDA context" must be initialized
    // This happens on the very first call to a CUDA function, and takes some time (around half a second)
    // We will do it right here, so that the run time measurements are accurate
    cudaDeviceSynchronize();  CUDA_CHECK;
    
    string image = "";
    bool ret = getParam("i", image, argc, argv);
    if (!ret) cerr << "ERROR: no image specified" << endl;
    if (argc <= 1) { cout << "Usage: " << argv[0] << " -i <image> -d <depth_image>" << endl; return 1; }
    
    string depth_image = "";
    ret = getParam("d", depth_image, argc, argv);
    if (!ret) cerr << "ERROR: no image specified" << endl;
    if (argc <= 1) { cout << "Usage: " << argv[0] << " -i <image> -d <depth_image>" << endl; return 1; }
    
    ImageRGB rgbIn(image, true);
    
    // raw_depth is 16bit integer
    ImageDepth rawDepth(depth_image, false);
    
    // get image dimensions
    uint32_t w = rawDepth.GetWidth();         // width
    uint32_t h = rawDepth.GetHeight();         // height
    
    uint32_t nc = rgbIn.GetChannels(); //number channels
    
    // convert to float and scale by 1/5 to get depth in millimeters
    ImageGS depth(w, h);
    rawDepth.ConvertAndScale((*depth.GetMatrixCV()), 0.2f);
    depth.SetTo(std::numeric_limits<float>::quiet_NaN(), (*rawDepth.GetMatrixCV()), 0);
    
    ImageGS depthOut(w, h);
    
    ImageGS intensity(w, h);
    
    shading = new ImageGS(w, h);
    
    ImageGS beta(w, h);
    
    ImageGS intenMinShTimesRho(w, h);
    
    rho = new ImageGS(w, h);
    
    normals = new ImageRGB(w, h);
    
    refinedDepth = new ImageGS(w, h);
    
    
    
    
    // DO COMPUTATION
    
    int32_t radius;
    uint32_t hw;
    uint32_t amountValidPixels;
    vector<PixelCoord> PixelCoords;
    
    Timer timer; timer.start();
    // setup start
    float* kernel = cpuComputeGaussianKernel(&hw, &radius);
    amountValidPixels = countValidPixels(*depth.GetMatrixCV(), PixelCoords);
    gpuRefine = new GPUilluminationRefinement(depth.GetRawDataPtr(), rgbIn.GetRawDataPtr(), kernel, h, w, nc, radius, amountValidPixels, PixelCoords);
    std::string wndName("Result");
    namedWindow(wndName.c_str(), 1);
    // trackbars
    createTrackbar("Lambda Rho", wndName.c_str(), &lambdaRhoSlider, 50, onTrackbarLambdaRho);
    createTrackbar("LambdaZ1", wndName.c_str(), &lambdaZ1Slider, 10000, onTrackbarLambdaZ1);
    createTrackbar("LambdaZ2", wndName.c_str(), &lambdaZ2Slider, 10000, onTrackbarLambdaZ2);
    createTrackbar("Surface Refinement Iterations", wndName.c_str(), &iterationSlider, 20, onTrackbarIterations);
    createTrackbar("Algorithm iterations", wndName.c_str(), &algIterationSlider, 20, onTrackbarIterations);
    // setup done
    
    timer.end(); float t = timer.get();  // elapsed time in seconds
    cout << "time init: " << t*1000 << " ms" << endl;
    
    
    
    gpuRefine->applyBilateralFilter();
    gpuRefine->computeIntensity();
    //gpuRefine->createPixelIndex();
    //gpuRefine->estimateSH1Lighting();
    //gpuRefine->cusolverLinearSystem();
    //gpuRefine->visualizeSH1Lighting();
    //gpuRefine->checkNormalComputation();
    //gpuRefine->multiplyIntensityWithShading();
    //gpuRefine->createValidIntensityAndHashmap();
    //gpuRefine->precomputeWeights();
    //onTrackbarLambdaRho(lambdaRhoSlider, 0);
    //gpuRefine->cuspComputeRho(25.f);
    //gpuRefine->multiplyShadingWithRhoAndSubstractFromIntensity();
    //gpuRefine->createValidIntensityMinusShadingTimesRho();
    //gpuRefine->cuspComputeBeta(1.f, 1.f);
    //gpuRefine->subtractBetaFromIntensity();
    //gpuRefine->computeRefinedDepth(2, 0.004, 0.0075);
    //gpuRefine->checkNormalComputationRefinedDepth();
    //onTrackbarLambdaZ1(lambdaZ1Slider, 0);
    //onTrackbarLambdaZ2(lambdaZ2Slider, 0);
    //onTrackbarIterations(iterationSlider, 0);
    
    
    gpuRefine->download(depthOut, intensity, *normals, intenMinShTimesRho, beta);

    // COMPUTATION FINISHED 
    
    
    
    // show Images
    rawDepth.Show("Raw Depth", 100, 100);
    intensity.Show("Intensity", 100 + (w + 40), 100);
    //rgbIn.Show("RGB Input", 100 + (w + 40) * 2, 100);
    
    // wait for key inputs
    cv::waitKey(0);
    
    // clean up
    delete[] kernel;
    delete gpuRefine;
    delete rho;
    delete shading;
    delete normals;
    delete refinedDepth;
    
    // close all opencv windows
    cvDestroyAllWindows();
    return 0;
}
