### Shading-Based Refinement of Depth Images ###
CUDA based implementation developed during "GPU Programming in Computer Vision" at Technische Universität München.

Our code is inspired by the following papers:

* R. Or-El, G. Rosman, A. Wetzler, R. Kimmel and A. M. Bruckstein,
"RGBD-fusion: Real-time high precision depth recovery", CVPR 2015
* C. Wu, M. Zollhöfer, M. Nießner, M. Stamminger, S. Izadi, C. Theobalt,
"Real-time Shading-based Refinementfor Consumer Depth Cameras", SIGGRAPH 2014