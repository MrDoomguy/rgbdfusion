#ifndef _DATATERM_LINEAR_OPERATOR_H_
#define _DATATERM_LINEAR_OPERATOR_H_

#include <cusp/linear_operator.h>
#include <stdio.h>

#include "computeWeightHelper.cu"

__global__ 
void datatermKernel(int amountValidPixels, int h, int w, float lambdaZ1, float lambdaZ2, const float* b, float* result, float* rho, PixelCoord* PixelCoords, int32_t* indexMap, float* weights, float* m, RightAndLowerWeights* weightsOne)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x; // valid space, i.e. only valid pixels

    if (idx < amountValidPixels)
    {
        PixelCoord &px = PixelCoords[idx];
        // linear index into 2D grid
        uint32_t index = w * px.y + px.x; // Image space, i.e. all pixels, not only valid ones
        int pos;
        
        float sum = 0.f, sumLap = 0.f;
        
        if (px.y > 0    ) {
            if(index - w > 310000) printf("upper %d\n", index);
            pos = indexMap[index - w]; // upper neighbor
            if(pos > -1) sum += CHECK_NAN((- m[1] * rho[index - w] * rho[index - w] * weights[index - w] * weights[index - w] * (m[0] + m[1]))) * b[pos];
        }
        if (px.y < h - 1) {
            if(index + w > 310000) printf("lower %d\n", index);
            pos = indexMap[index + w];
            if(pos > -1) sum += CHECK_NAN((- m[1] * rho[index] * rho[index] * weights[index] * weights[index] * (m[0] + m[1]))) * b[pos];  // lower neighbor
        }
        if (px.x > 0 ) {
            if(index - 1 > 310000) printf("left %d\n", index);
            pos = indexMap[index - 1];
            if(pos > -1) sum += CHECK_NAN((- m[0] * rho[index - 1] * rho[index - 1] * weights[index - 1] * weights[index - 1] * (m[0] + m[1]))) * b[pos];  // left neighbor
        }
        if (px.x < w - 1) {
            if(index + 1 > 310000) printf("right %d\n", index);
            pos = indexMap[index + 1];
            if(pos > -1) sum += CHECK_NAN((- m[0] * rho[index] * rho[index] * weights[index] * weights[index] * (m[0] + m[1]))) * b[pos];  // right neighbor
        }
        
        
        
        if (px.y > 0 && px.x < w - 1) {
            if(index - w + 1 > 310000) printf("upper right %d\n", index);
            pos = indexMap[index - w + 1];
            if(pos > -1) sum += CHECK_NAN(( m[0] * m[1] * rho[index - w]  *rho[index - w] * weights[index - w] * weights[index - w] )) * b[pos];  // upper right neighbor
        }
        if (px.x > 0 && px.y < h - 1) {
            if(index + w - 1 > 310000) printf("lower left %d\n", index);
            pos = indexMap[index + w - 1];
            if(pos > -1) sum += CHECK_NAN(( m[0] * m[1] * rho[index - 1] * rho[index - 1] * weights[index - 1] * weights[index - 1] )) * b[pos];  // lower left neighbor
        }
        
        
        // center
        if(px.x > 0) {
            sum += m[0]*m[0] * rho[index - 1] * rho[index - 1] * weights[index - 1] * weights[index - 1] * b[idx];
        }
        if(px.y > 0) {
            sum += m[1]*m[1] * rho[index - w] * rho[index - w] * weights[index - w] * weights[index - w] * b[idx];
        }
        sum += (m[0] + m[1]) * (m[0] + m[1]) * rho[index] * rho[index] * weights[index] * weights[index] * b[idx];
        
        
        
        
        // Laplacian
        
        
        if (px.y > 0    ) {
            pos = indexMap[index - w];
            if(pos > -1) sumLap += computeWeightForInnerCross(weightsOne, index - w, 0, -1, w, h, px.x, px.y) * b[pos];  // upper neighbor
        }
        if (px.y < h - 1) {
            pos = indexMap[index + w];
            if(pos > -1) sumLap += computeWeightForInnerCross(weightsOne, index + w, 0, 1, w, h, px.x, px.y) * b[pos];  // lower neighbor
        }
        if (px.x > 0 ) {
            pos = indexMap[index - 1];
            if(pos > -1) sumLap += computeWeightForInnerCross(weightsOne, index - 1, -1, 0, w, h, px.x, px.y) * b[pos];  // left neighbor
        }
        if (px.x < w - 1) {
            pos = indexMap[index + 1];
            if(pos > -1) sumLap += computeWeightForInnerCross(weightsOne, index + 1, 1, 0, w, h, px.x, px.y) * b[pos];  // right neighbor
        }
        
        
        
        
        if (px.y > 1    ) {
            pos = indexMap[index - 2 * w];
            if(pos > -1) sumLap += computeWeighForOuterCross(weightsOne, index - 2 * w, 0, -2, w) * b[pos];  // upper upper neighbor
        }
        if (px.y < h - 2) {
            pos = indexMap[index + 2 * w];
            if(pos > -1) sumLap += computeWeighForOuterCross(weightsOne, index + 2 * w, 0, 2, w) * b[pos];  // lower lower neighbor
        }
        if (px.x > 1    ) {
            pos = indexMap[index - 2];
            if(pos > -1) sumLap += computeWeighForOuterCross(weightsOne, index - 2, -2, 0, w) * b[pos];  // left left neighbor
        }
        if (px.x < w - 2) {
            pos = indexMap[index + 2];
            if(pos > -1) sumLap += computeWeighForOuterCross(weightsOne, index + 2, 2, 0, w) * b[pos];  // right right neighbor
        }
        
        
        
        
        if (px.y > 0 && px.x > 0) {
            pos = indexMap[index - w - 1];
            if(pos > -1) sumLap += computeWeightForDiagonal(weightsOne, index - w - 1, -1, -1, w) * b[pos];  // upper left neighbor
        }
        if (px.y > 0 && px.x < w - 1) {
            pos = indexMap[index - w + 1];
            if(pos > -1) sumLap += computeWeightForDiagonal(weightsOne, index - w + 1, 1, -1, w) * b[pos];  // upper right neighbor
        }
        if (px.x > 0 && px.y < h - 1) {
            pos = indexMap[index + w - 1];
            if(pos > -1) sumLap += computeWeightForDiagonal(weightsOne, index + w - 1, -1, 1, w) * b[pos];  // lower left neighbor
        }
        if (px.x < w - 1 && px.y < h - 1) {
            pos = indexMap[index + w + 1];
            if(pos > -1) sumLap += computeWeightForDiagonal(weightsOne, index + w + 1, 1, 1, w) * b[pos];  // lower right neighbor
        }
        
        
        
        
        sumLap *= lambdaZ2;
        sumLap += (lambdaZ2 * computeWeightForCenter(weightsOne, index, w, h, px.x, px.y)) * b[idx];
        
        
        
        
        
        // write result
        result[idx] = sum + sumLap;
        result[idx] += lambdaZ1 * 1.f * b[idx];
    }
}

class dataterm : public cusp::linear_operator<float,cusp::device_memory>
{
public:
    typedef cusp::linear_operator<float,cusp::device_memory> super;

    int amountValidPixels, h, w;
    float lambdaZ1, lambdaZ2;
    dim3 grid, block;
    float* rho;
    PixelCoord* PixelCoords;
    int32_t* indexMap;
    float* weights;
    float* m;
    RightAndLowerWeights* weightsOne;

    // constructor
    dataterm(int amountValidPixels, int h, int w, float lambdaZ1, float lambdaZ2, dim3 grid, dim3 block, float* _rho, PixelCoord* _PixelCoords, int32_t* _indexMap, float* _weights, float* _m, RightAndLowerWeights* _weightsOne)
        : super(amountValidPixels, amountValidPixels), amountValidPixels(amountValidPixels), h(h), w(w), lambdaZ1(lambdaZ1), lambdaZ2(lambdaZ2), grid(grid), block(block) {
        
        rho = _rho;
        PixelCoords = _PixelCoords;
        indexMap = _indexMap;
        weights = _weights;
        m = _m;
        weightsOne = _weightsOne;
    }

    // linear operator y = A*x
    template <typename VectorType1,
             typename VectorType2>
    void operator()(const VectorType1& b, VectorType2& result) const
    {
        // obtain a raw pointer to device memory
        const float * b_ptr = thrust::raw_pointer_cast(&b[0]);
        float * result_ptr = thrust::raw_pointer_cast(&result[0]);

        datatermKernel<<<grid, block>>>(amountValidPixels, h, w, lambdaZ1, lambdaZ2, b_ptr, result_ptr, rho, PixelCoords, indexMap, weights, m, weightsOne);
    }
};

#endif
