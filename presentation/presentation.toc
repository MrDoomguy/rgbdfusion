\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Introduction and Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Algorithm}{5}{0}{2}
\beamer@sectionintoc {3}{Implementation Details}{13}{0}{3}
\beamer@sectionintoc {4}{Live Demo}{15}{0}{4}
\beamer@sectionintoc {5}{Q\&A}{17}{0}{5}
