#ifndef _RHO_LINEAR_OPERATOR_H_
#define _RHO_LINEAR_OPERATOR_H_

#include <cusp/linear_operator.h>
#include <stdio.h>

#include "computeWeightHelper.cu"


__global__
void rhoBetaKernel(const int amountValidPixels, const int h, const int w, float lambda1, float lambda2, const float * b, float * result, float* shading, PixelCoord* PixelCoords, int32_t* indexMap, RightAndLowerWeights* weights)
{
    int idx = blockDim.x * blockIdx.x + threadIdx.x; // valid space, i.e. only valid pixels

    if (idx < amountValidPixels)
    {
        PixelCoord &px = PixelCoords[idx];
        // linear index into 2D grid
        size_t index = w * px.y + px.x; // Image space, i.e. all pixels, not only valid ones
        float sh = shading[index];
        int pos;
        
        float sum = 0.f;
        
        if (px.y > 0    ) {
            pos = indexMap[index - w];
            if(pos > -1) sum += CHECK_NAN(computeWeightForInnerCross(weights, index - w, 0, -1, w, h, px.x, px.y)) * b[pos];  // upper neighbor
        }
        if (px.y < h - 1) {
            pos = indexMap[index + w];
            if(pos > -1) sum += CHECK_NAN(computeWeightForInnerCross(weights, index + w, 0, 1, w, h, px.x, px.y)) * b[pos];  // lower neighbor
        }
        if (px.x > 0 ) {
            pos = indexMap[index - 1];
            if(pos > -1) sum += CHECK_NAN(computeWeightForInnerCross(weights, index - 1, -1, 0, w, h, px.x, px.y) )* b[pos];  // left neighbor
        }
        if (px.x < w - 1) {
            pos = indexMap[index + 1];
            if(pos > -1) sum += CHECK_NAN(computeWeightForInnerCross(weights, index + 1, 1, 0, w, h, px.x, px.y)) * b[pos];  // right neighbor
        }
        
        
        
        
        if (px.y > 1    ) {
            pos = indexMap[index - 2 * w];
            if(pos > -1) sum += CHECK_NAN(computeWeighForOuterCross(weights, index - 2 * w, 0, -2, w) )* b[pos];  // upper upper neighbor
        }
        if (px.y < h - 2) {
            pos = indexMap[index + 2 * w];
            if(pos > -1) sum += CHECK_NAN(computeWeighForOuterCross(weights, index + 2 * w, 0, 2, w)) * b[pos];  // lower lower neighbor
        }
        if (px.x > 1    ) {
            pos = indexMap[index - 2];
            if(pos > -1) sum += CHECK_NAN(computeWeighForOuterCross(weights, index - 2, -2, 0, w)) * b[pos];  // left left neighbor
        }
        if (px.x < w - 2) {
            pos = indexMap[index + 2];
            if(pos > -1) sum += CHECK_NAN(computeWeighForOuterCross(weights, index + 2, 2, 0, w)) * b[pos];  // right right neighbor
        }
        
        
        
        
        if (px.y > 0 && px.x > 0) {
            pos = indexMap[index - w - 1];
            if(pos > -1) sum += CHECK_NAN(computeWeightForDiagonal(weights, index - w - 1, -1, -1, w) )* b[pos];  // upper left neighbor
        }
        if (px.y > 0 && px.x < w - 1) {
            pos = indexMap[index - w + 1];
            if(pos > -1) sum += CHECK_NAN(computeWeightForDiagonal(weights, index - w + 1, 1, -1, w) )* b[pos];  // upper right neighbor
        }
        if (px.x > 0 && px.y < h - 1) {
            pos = indexMap[index + w - 1];
            if(pos > -1) sum += CHECK_NAN(computeWeightForDiagonal(weights, index + w - 1, -1, 1, w) )* b[pos];  // lower left neighbor
        }
        if (px.x < w - 1 && px.y < h - 1) {
            pos = indexMap[index + w + 1];
            if(pos > -1) sum += CHECK_NAN(computeWeightForDiagonal(weights, index + w + 1, 1, 1, w) )* b[pos];  // lower right neighbor
        }
        
        if(lambda2 < 0) {
            // compute Rho
            sum *= lambda1;
            sum += (lambda1 * CHECK_NAN(computeWeightForCenter(weights, index, w, h, px.x, px.y)) + sh*sh) * b[idx];
        } else {
            // compute Beta
            sum *= lambda1;
            sum += (lambda1 * CHECK_NAN(computeWeightForCenter(weights, index, w, h, px.x, px.y)) + (lambda2 * 1.f + 1.f) ) * b[idx];
        }
        
        
        // write result
        result[idx] = sum;
    }
}

class rhoBeta : public cusp::linear_operator<float,cusp::device_memory>
{
public:
    typedef cusp::linear_operator<float,cusp::device_memory> super;

    int amountValidPixels, h, w;
    float lambda1, lambda2;
    dim3 grid, block;
    float* shading;
    PixelCoord* PixelCoords;
    int32_t* indexMap;
    RightAndLowerWeights* weights;

    // constructor
    rhoBeta(int amountValidPixels, int h, int w, float lambda1, float lambda2, dim3 grid, dim3 block, float* _shading, PixelCoord* _PixelCoords, int32_t* _indexMap, RightAndLowerWeights* _weights)
        : super(amountValidPixels, amountValidPixels), amountValidPixels(amountValidPixels), h(h), w(w), lambda1(lambda1), lambda2(lambda2), grid(grid), block(block) {
        
        shading = _shading;
        PixelCoords = _PixelCoords;
        indexMap = _indexMap;
        weights = _weights;
    }

    // linear operator y = A*x
    template <typename VectorType1,
             typename VectorType2>
    void operator()(const VectorType1& b, VectorType2& result) const
    {
        // obtain a raw pointer to device memory
        const float * b_ptr = thrust::raw_pointer_cast(&b[0]);
        float * result_ptr = thrust::raw_pointer_cast(&result[0]);

        rhoBetaKernel<<<grid, block>>>(amountValidPixels, h, w, lambda1, lambda2, b_ptr, result_ptr, shading, PixelCoords, indexMap, weights);
    }
};

#endif
