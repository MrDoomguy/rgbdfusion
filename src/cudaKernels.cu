#ifndef _CUDA_KERNELS_H_
#define _CUDA_KERNELS_H_

#include <cstdint>


// TODO load kernel into constant memory
__global__ void bilateralFilterKernel(float* depth_in, float* depth_out, float* kernel, int32_t rad, uint32_t h, uint32_t w, float factor) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        uint32_t idx = x + (size_t)w*y;
        const float& d = depth_in[idx];
        if (d != d) {
            depth_out[idx] = nanf("1");
        } else {
            float d_sum = d;
            float w_sum = 1.0f;
            
            for(int32_t xi = -rad; xi <= rad; xi++) {
                for(int32_t yi = -rad; yi <= rad; yi++) {
                    if(xi == 0 && yi == 0) continue;
                    
                    int32_t new_x = x + xi, new_y = y + yi;
                    if(new_x < 0 || new_y < 0 || new_x >= w ||  new_y >= h) continue;
                    
                    const float& curr_d = depth_in[new_x + (size_t)w*new_y];
                    
                    if(curr_d == curr_d) {
                        float diff = d - curr_d;
                        float weight = kernel[xi+rad + (size_t)(yi+rad)*(2*rad+1)] * expf(factor * (diff * diff));
                        d_sum += weight * curr_d;
                        w_sum += weight;
                    }                    
                }
            }
            depth_out[idx] = d_sum / w_sum;
        }
    } 
}

__global__ void computeIntensityKernel(float* imgIn, float* intensity, int h, int w, float nc) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        float intensitySum = 0;
        for(uint32_t c = 0; c < nc; c++) {
            size_t idx = x + (size_t)w*y + (size_t)w*h*c;
            intensitySum += imgIn[idx];
        }
        float i = intensitySum / nc;
        
        intensity[x + (size_t)w*y] = i;
    }
}

__global__ void createPixelIndexKernel(float* index, float* depth, uint32_t h, uint32_t w) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        size_t idx = x + (size_t)w*y;
        if(depth[idx] != depth[idx]) {
            index[idx] = -1.f;
        } else {
            float dx = depth[(x + 1) + (size_t)w*y] - depth[idx];
            float dy = depth[x + (size_t) w * (y + 1)] - depth[idx];

            if(dx == dx && dy == dy && abs(dx) < 0.05 * 1000.0 && abs(dy) < 0.05 * 1000.0) {
                index[idx] = idx;
            } else {
                index[idx] = -1.f;
            }
        }
    }
}

__global__ void createValidPixelsAndHashmapKernel(float* in, float* out, uint32_t w, uint32_t amountValidPixels, PixelCoord* PixelCoords, int32_t* hashmap) {
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < amountValidPixels) {
        PixelCoord& px = PixelCoords[idx];
        out[idx] = in[px.x + w * px.y];
        hashmap[px.x + w * px.y] = px.pos;
        //if(out[idx] != out[idx]) printf("nan in out: %d\n", px.pos);
        //if(in[px.x + w * px.y] != in[px.x + w * px.y]) printf("nan in in: %d\n", px.x + w * px.y);
    }
}

__device__ void getNormalizedNormal(float* n, float dx, float dy) {
    n[0] = dx;
    n[1] = dy;
    n[2] = -1.f;
    
    // normalize
    float sum = 1.f;
    #pragma unroll
    for(uint32_t k = 0; k < 2; k++) {
        sum += n[k]*n[k];
    }
    sum = sqrtf(sum);
    #pragma unroll
    for(uint32_t k = 0; k < 3; k++) {
        n[k] /= sum;
    }
    // normalize done
}

__global__ void estimateSH1LightingKernel(float* index, float* intensity, float* depth, float* A, float* b, uint32_t h, uint32_t w) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        float n[4] = {0.f, 0.f, 0.f, 1.f};
        
        size_t idx = x + (size_t)w*y;
        if(index[idx] >= 0.f) {
            const float &i = intensity[idx];

            if(i <= 0.95f && i >= 0.05f) {
                float dx = depth[idx + 1] - depth[idx];
                float dy = depth[idx + w] - depth[idx];
                
                getNormalizedNormal(n, dx, dy);
                
                for(uint32_t ai = 0; ai < 4; ai++) {
                    for(uint32_t aj = 0; aj < ai+1; aj++) {
                        atomicAdd(&A[ai + 4 * aj], n[ai]*n[aj]);
                    }
                }
                atomicAdd(&b[0], n[0] * i);
                atomicAdd(&b[1], n[1] * i);
                atomicAdd(&b[2], n[2] * i);
                atomicAdd(&b[3], n[3] * i);
            }
        }
    }
}

__global__ void visualizeSH1LightingKernel(float* S, float* depth, float* index, float* m, uint32_t h, uint32_t w) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        float n[4] = {0.f, 0.f, 0.f, 1.f};

        uint32_t idx = x + w*y;
        if(index[idx] >= 0.f) {
            float dx = depth[(x + 1) + w*y] - depth[idx];
            float dy = depth[x + w*(y + 1)] - depth[idx];
            
            getNormalizedNormal(n, dx, dy);
            
            S[idx] = n[0]*m[0] + n[1]*m[1] + n[2]*m[2] + n[3]*m[3];
            if(S[idx] != S[idx]) printf("x: %d y: %d w: %d idx: %d\n",x, y, w, idx);
        } else {
            S[idx] = nanf("1");
        }
    }
}

__global__ void checkNormalsKernel(float* d_normals, float* depth, float* index, uint32_t h, uint32_t w) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        float n[3] = {0.f, 0.f, 0.f};

        size_t idx = x + (size_t)w*y;
        if(index[idx] >= 0.f) {
            float dx = depth[(x + 1) + (size_t)w*y] - depth[idx];
            float dy = depth[x + (size_t) w * (y + 1)] - depth[idx];
            
            getNormalizedNormal(n, dx, dy);
            
            d_normals[x + (size_t)w*y + (size_t)w*h*2] = n[0];
            d_normals[x + (size_t)w*y + (size_t)w*h*1] = n[1];
            d_normals[x + (size_t)w*y + (size_t)w*h*0] = n[2];
        } else {
            d_normals[x + (size_t)w*y + (size_t)w*h*2] = nanf("1");
            d_normals[x + (size_t)w*y + (size_t)w*h*1] = nanf("1");
            d_normals[x + (size_t)w*y + (size_t)w*h*0] = nanf("1");
        }
    }
}

__global__ void copyBackKernel(float* outBuffer, float* srcBuffer, PixelCoord* PixelCoords, uint32_t amountValidPixels, int w)
{
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < amountValidPixels) {
        PixelCoord& px = PixelCoords[idx];
        outBuffer[px.x + w*px.y] = srcBuffer[px.pos];
    }
}

__global__ void precomputeWeightsKernel(float* intensity, float* depth, RightAndLowerWeights* weights, RightAndLowerWeights* weightsOne, PixelCoord* PixelCoords, int32_t* hashmap, uint32_t h, uint32_t w, uint32_t amountValidPixels) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    if (x<amountValidPixels) {
        float inv_two_sigma_d_sqr = 1.0 / (2.0 * 50), inv_two_sigma_c_sqr = 1.0 / (2.0 * 0.1), tau = 0.3f;
        PixelCoord &px = PixelCoords[x];
        uint32_t idx = px.x + w*px.y;
        
        if(hashmap[idx] > -1) {
        
            const float &i = intensity[idx];
            const float &d = depth[idx];
            float iLower = nanf("1");
            float dLower = nanf("1");
            float iRight = nanf("1");
            float dRight = nanf("1");
        
            if(px.y < h - 1 && hashmap[idx + w] > -1) {
                iLower = intensity[idx + w];
                dLower = depth[idx + w];
            }
            if(px.x < w - 1 && hashmap[idx + 1] > -1) {
                iRight = intensity[idx + 1];
                dRight = depth[idx + 1];
            }
            
            float delta_iLower = i - iLower, delta_iRight = i - iRight;
            delta_iLower *= delta_iLower;
            delta_iRight *= delta_iRight;
            
            if (!(delta_iRight > tau || dRight != dRight)) {
                float delta_dRight = d - dRight;
                delta_dRight *= delta_dRight;
                weights[idx].right = expf(-delta_dRight * inv_two_sigma_d_sqr -delta_iRight * inv_two_sigma_c_sqr);
            }
            if (!(delta_iLower > tau || dLower != dLower)) {
                float delta_dLower = d - dLower;
                delta_dLower *= delta_dLower;
                weights[idx].lower = expf(-delta_dLower * inv_two_sigma_d_sqr -delta_iLower * inv_two_sigma_c_sqr);
            }
            if (dRight == dRight) {
                weightsOne[idx].right = 1.f;
            }
            if (dLower == dLower) {
                weightsOne[idx].lower = 1.f;
            }
            //if(x == w - 1 || y == h - 1) printf("border right %f lower %f\n", weights[idx].right, weights[idx].lower);
            /*if(x == w - 2) {
                weightsOne[idx].right = 0.f;
                //weights[idx].right = 0.f;
            }
            if(y == h - 2) {
                weightsOne[idx].lower = 0.f;
                weights[idx].lower = 0.f;
            }*/
        }
    }
}

__global__ void multiplyShadingWithRhoAndSubstractFromIntensityKernel(float* shading, float* rho, float* intensity, float* intenMinShTimesRho, uint32_t h, uint32_t w) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        size_t idx = x + w*y;
        intenMinShTimesRho[idx] = intensity[idx] - shading[idx] * rho[idx];
    }
}

__global__ void multiplyMatricesElementwiseKernel(float* inA, float* inB, float* out, uint32_t h, uint32_t w) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        size_t idx = x + w*y;
        out[idx] = inA[idx] * inB[idx];
        //if(inA[idx] != inA[idx]) printf("nan in inA: %d\n", idx);
        //if(inB[idx] != inB[idx]) printf("nan in inB: %d\n", idx);
    }
}

//minuend - subtrahend = difference
__global__ void subtractMatricesElementwiseKernel(float* minuend, float* subtrahend, float* diff, uint32_t h, uint32_t w) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        size_t idx = x + w*y;
        diff[idx] = minuend[idx] - subtrahend[idx];
    }
}

__global__ void addMatricesElementwiseKernel(float* inA, float* inB, float* out, uint32_t h, uint32_t w) {
    int x = threadIdx.x + blockDim.x * blockIdx.x;
    int y = threadIdx.y + blockDim.y * blockIdx.y;
    if (x<w && y<h) {
        size_t idx = x + w*y;
        out[idx] = inA[idx] + inB[idx];
    }
}

__global__ void PrecomputeWeightsDatatermKernel(float* validDepthPixels,float* weights,
        PixelCoord* PixelCoords, int32_t* indexMap, uint32_t h, uint32_t w, uint32_t amountValidPixels) {
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < amountValidPixels) {
        float& d = validDepthPixels[idx];
        PixelCoord &px = PixelCoords[idx];
        // linear index into 2D grid
        size_t index = w * px.y + px.x; // Image space, i.e. all pixels, not only valid ones
        
        int posR, posL;
        
        float z_dx = 0.f, z_dy = 0.f, z_dz = -1.f;
        if(px.x < w - 1 && px.y < h - 1) {
            posR = indexMap[index + 1];
            posL = indexMap[index + w];
            if(posR > -1) z_dx = validDepthPixels[posR] - d;
            if(posL > -1) z_dy = validDepthPixels[posL] - d;
        }
        
        /*if(px.y < h - 1) {
            pos = indexMap[index + w];
            if(pos > -1) z_dy = validDepthPixels[pos] - d;
        }*/
        weights[index] = 1.0f / sqrtf(z_dz* z_dz + z_dx * z_dx + z_dy * z_dy);
    }
}

__global__ void precomputeBForDatatermKernel(float* validDepthPixels, float* rho, float* intensity, float* m, float* b, float* weights,
        PixelCoord* PixelCoords, int32_t* indexMap, uint32_t h, uint32_t w, uint32_t amountValidPixels, float lambdaZ1) {
    int idx = threadIdx.x + blockDim.x * blockIdx.x;
    if (idx < amountValidPixels) {
        PixelCoord &px = PixelCoords[idx];
        // linear index into 2D grid
        size_t index = w * px.y + px.x; // Image space, i.e. all pixels, not only valid ones
        
        float& i = intensity[index];
        float& r = rho[index];
        float& weight = weights[index];
        float& d = validDepthPixels[idx];
        float z_dz = -1.f;
        
        //printf("%f %f %f %f\n", m[0], m[1], m[2], m[3]);
        
        float sum = 0.f;
        if/*(weights[index + 1] > 0 && weights[index + w] > 0)*/ (px.x > 0 && px.y > 0) {
            sum += (-r * m[0] * weight - r * m[1] * weight) * (-(weight * m[2] * z_dz * r + r * m[3] - i)); // center
            sum += (r * m[0] * weight) * (-(weights[index - 1] * m[2] * z_dz * rho[index - 1] + rho[index - 1] * m[3] - intensity[index - 1])); // left
            sum += (r * m[1] * weight) * (-(weights[index - w] * m[2] * z_dz * rho[index - w] + rho[index - w] * m[3] - intensity[index - w])); // upper
        }
        
        b[idx] = sum + lambdaZ1 * d;
        
        if(b[idx]!=b[idx])printf("nan at %d\n", idx);
        //if(b[idx] != b[idx]) printf("b: %f sum: %f weightRight: %f wightLower: %f inten: %f rho: %f valDepth: %f\n", b[idx], sum, weights[index + 1], weights[index + w], i, r, d);
    }
}


#endif
