#ifndef _GPU_ILLUMINATION_REFINEMENT_H_
#define _GPU_ILLUMINATION_REFINEMENT_H_

#include <iostream>
#include "helper.h"
#include "Image.h"
#include "CudaBuffer.h"
#include "cudaKernels.cu"
#include "RhoLinearOperator.cu"
#include "DatatermLinearOperator.cu"
#include "cublas_v2.h"
#include "cusolverDn.h"
#include <cusp/krylov/cg.h>

using namespace std;

class GPUilluminationRefinement {
    private:
        uint32_t hw;
        uint32_t h;
        uint32_t w;
        uint32_t nc;
        uint32_t amountValidPixels;
        int32_t radius;
        float factor;
        float* kernel = NULL;
        
        vector<PixelCoord> PixelCoords;
        
        cusolverDnHandle_t handle;
            
        CudaBuffer<float> d_pixelIndex;
        CudaBuffer<float> d_rgbIn;
        CudaBuffer<float> d_intensity;
        CudaBuffer<float> d_depthIn;
        CudaBuffer<float> d_depthOut;
        CudaBuffer<float> d_kernel;
        CudaBuffer<float> d_A;
        CudaBuffer<float> d_b;
        CudaBuffer<float> d_m;
        CudaBuffer<float> d_S;
        CudaBuffer<float> d_rho;
        CudaBuffer<float> d_normals;
        CudaBuffer<PixelCoord> d_PixelCoords;
        CudaBuffer<float> d_validIntensityPixels;
        CudaBuffer<int32_t> d_PixelCoordsToPosHashmap;
        CudaBuffer<RightAndLowerWeights> d_weights;
        CudaBuffer<RightAndLowerWeights> d_weightsOne;
        CudaBuffer<float> d_IntensityMinusShadingTimesRho;
        CudaBuffer<float> d_beta;
        CudaBuffer<float> d_intensityTimesShading;
        CudaBuffer<float> d_validIntensityMinusShadingTimesRhoPixels;
        CudaBuffer<float> d_intensityMinusBeta;
        CudaBuffer<float> d_validDepthPixels;
        CudaBuffer<float> d_zLast;
        CudaBuffer<float> d_zLaplacian;
        CudaBuffer<float> d_bDataterm;
        CudaBuffer<float> d_weightsDataterm;
        CudaBuffer<float> d_dataterm;
        
        dim3 block;
        dim3 grid;
        dim3 block1D;
        dim3 grid1D;
        
    public:
        // Contructor
        GPUilluminationRefinement(float* depth_in, float* rgb_in, float* newKernel, uint32_t newH, uint32_t newW, uint32_t newNc, uint32_t newRadius, uint32_t newAmountValidPixels, vector<PixelCoord> &newPixelCoords) {
            h = newH;
            w = newW;
            nc = newNc;
            amountValidPixels = newAmountValidPixels;
            radius = newRadius;
            hw = 2*radius+1;
            factor = -0.5f / (50.0f * 50.0f);
            
            PixelCoords = newPixelCoords;
            
            CUSOLVER_CHECK(cusolverDnCreate(&handle));
            
            kernel = (float*)malloc(sizeof(float)*hw*hw);
            memcpy(kernel, newKernel, sizeof(float)*hw*hw);
            
            d_pixelIndex.CreateBuffer(w*h);
            
            d_rgbIn.CreateBuffer(w*h*nc);
            d_rgbIn.UploadData(rgb_in);
            
            d_intensity.CreateBuffer(w*h);
            d_intensity.SetBytewiseValue(0);
            
            d_depthIn.CreateBuffer(w*h);
            d_depthIn.UploadData(depth_in);
            
            d_depthOut.CreateBuffer(w*h);
            d_depthOut.SetBytewiseValue(0);
            
            d_kernel.CreateBuffer(hw*hw);
            d_kernel.UploadData(kernel);
            
            d_A.CreateBuffer(16);
            d_A.SetBytewiseValue(0);
            
            d_b.CreateBuffer(4);
            d_b.SetBytewiseValue(0);
            
            d_m.CreateBuffer(4);
            
            d_S.CreateBuffer(h*w);
            d_S.SetBytewiseValue(0);
            
            d_rho.CreateBuffer(h*w);
            d_rho.SetBytewiseValue(0);
            
            d_normals.CreateBuffer(h*w*nc);
            d_normals.SetBytewiseValue(0);
            
            d_PixelCoords.CreateBuffer((size_t)amountValidPixels);
            d_PixelCoords.UploadData(PixelCoords.data());
            
            d_validIntensityPixels.CreateBuffer(amountValidPixels);
            
            d_PixelCoordsToPosHashmap.CreateBuffer(h*w);
            d_PixelCoordsToPosHashmap.SetBytewiseValue(-1);
            
            d_weights.CreateBuffer(h*w);
            d_weights.SetBytewiseValue(0);
            d_weightsOne.CreateBuffer(h*w);
            d_weightsOne.SetBytewiseValue(0);
            
            d_IntensityMinusShadingTimesRho.CreateBuffer(h*w);
            
            d_beta.CreateBuffer(h*w);
            d_beta.SetBytewiseValue(0);
            
            d_intensityTimesShading.CreateBuffer(h*w);
            
            d_validIntensityMinusShadingTimesRhoPixels.CreateBuffer(h*w);
            d_validIntensityMinusShadingTimesRhoPixels.SetBytewiseValue(0);
            
            d_intensityMinusBeta.CreateBuffer(h*w);
            
            d_validDepthPixels.CreateBuffer(h*w);
            
            d_zLast.CreateBuffer(h*w);
            d_zLast.SetBytewiseValue(0);
            
            d_zLaplacian.CreateBuffer(h*w);
            d_zLaplacian.SetBytewiseValue(0);
            
            d_bDataterm.CreateBuffer(amountValidPixels);
            d_weightsDataterm.CreateBuffer(h*w);
            d_weightsDataterm.SetBytewiseValue(0);
            
            d_dataterm.CreateBuffer(h*w);
            d_dataterm.SetBytewiseValue(0);
            
            block = dim3(32, 32, 1); // 32*32 = 1024 threads
            // ensure enough blocks to cover w * h elements (round up)
            grid = dim3((w + block.x - 1) / block.x, (h + block.y - 1) / block.y, 1);
            block1D = dim3(1024, 1, 1);
            grid1D = dim3((amountValidPixels + block.x - 1) / block.x, 1, 1);
        };
        
        // Destructor
        ~GPUilluminationRefinement() {
        	d_pixelIndex.DestroyBuffer();
        	d_rgbIn.DestroyBuffer();
        	d_intensity.DestroyBuffer();
        	d_depthIn.DestroyBuffer();
        	d_depthOut.DestroyBuffer();
        	d_kernel.DestroyBuffer();
        	d_A.DestroyBuffer();
        	d_b.DestroyBuffer();
        	d_m.DestroyBuffer();
        	d_S.DestroyBuffer();
        	d_rho.DestroyBuffer();
        	d_normals.DestroyBuffer();
            d_PixelCoords.DestroyBuffer();
            d_validIntensityPixels.DestroyBuffer();
            d_PixelCoordsToPosHashmap.DestroyBuffer();
            d_weights.DestroyBuffer();
            d_IntensityMinusShadingTimesRho.DestroyBuffer();
            d_beta.DestroyBuffer();
            d_intensityTimesShading.DestroyBuffer();
            d_validIntensityMinusShadingTimesRhoPixels.DestroyBuffer();
            d_intensityMinusBeta.DestroyBuffer();
            d_weightsOne.DestroyBuffer();
            d_validDepthPixels.DestroyBuffer();
            d_zLast.DestroyBuffer();
            d_bDataterm.DestroyBuffer();
            d_dataterm.DestroyBuffer();
            d_weightsDataterm.DestroyBuffer();
        	
        	CUSOLVER_CHECK(cusolverDnDestroy(handle));
        
            delete[] kernel;
        };
        
        
        
        void applyBilateralFilter() {
            bilateralFilterKernel <<<grid, block>>> (d_depthIn.GetDevicePtr(), d_depthOut.GetDevicePtr(), d_kernel.GetDevicePtr(), radius, h, w, factor);
            CUDA_CHECK;
        }
        
        
        
        void createPixelIndex() {
            createPixelIndexKernel <<< grid, block >>> (d_pixelIndex.GetDevicePtr(), d_depthIn.GetDevicePtr(), h, w);
            CUDA_CHECK;
        }
        
        
        
        void multiplyIntensityWithShading() {
            multiplyMatricesElementwiseKernel <<< grid, block >>> (d_intensity.GetDevicePtr(), d_S.GetDevicePtr(), d_intensityTimesShading.GetDevicePtr(), h, w);
            CUDA_CHECK;
        }
        
        
        void subtractBetaFromIntensity() {
            subtractMatricesElementwiseKernel <<<grid, block>>> (d_intensity.GetDevicePtr(), d_beta.GetDevicePtr(), d_intensityMinusBeta.GetDevicePtr(), h, w);
            CUDA_CHECK;
        }
        
        
        
        void createValidIntensityAndHashmap() {
            createValidPixelsAndHashmapKernel <<<grid1D, block1D>>> (d_intensityTimesShading.GetDevicePtr(), d_validIntensityPixels.GetDevicePtr(), 
            w, amountValidPixels, d_PixelCoords.GetDevicePtr(), d_PixelCoordsToPosHashmap.GetDevicePtr());
            CUDA_CHECK;
        }
        
        void createValidIntensityMinusShadingTimesRho() {
            createValidPixelsAndHashmapKernel <<<grid1D, block1D>>> (d_IntensityMinusShadingTimesRho.GetDevicePtr(),
            d_validIntensityMinusShadingTimesRhoPixels.GetDevicePtr(), w, amountValidPixels, d_PixelCoords.GetDevicePtr(), d_PixelCoordsToPosHashmap.GetDevicePtr());
            CUDA_CHECK;
        }
        
        
        
        void computeIntensity() {
            computeIntensityKernel <<<grid, block>>> (d_rgbIn.GetDevicePtr(), d_intensity.GetDevicePtr(), h, w, nc);
            CUDA_CHECK;
        }
        
        
        
        void estimateSH1Lighting() {
            estimateSH1LightingKernel <<<grid, block>>> (d_pixelIndex.GetDevicePtr(), d_intensity.GetDevicePtr(), d_depthOut.GetDevicePtr(), 
            d_A.GetDevicePtr(), d_b.GetDevicePtr(), h, w);
            CUDA_CHECK;
        }
        
        

        void visualizeSH1Lighting() {
            visualizeSH1LightingKernel <<<grid, block>>> (d_S.GetDevicePtr(), d_depthOut.GetDevicePtr(), d_pixelIndex.GetDevicePtr(), d_b.GetDevicePtr(), h, w);
            CUDA_CHECK;
        }
        
        
        
        void checkNormalComputation() {
            checkNormalsKernel <<<grid, block>>> (d_normals.GetDevicePtr(), d_depthOut.GetDevicePtr(), d_pixelIndex.GetDevicePtr(), h, w);
            CUDA_CHECK;
        }
        
        
        
        void checkNormalComputationRefinedDepth() {
            checkNormalsKernel <<<grid, block>>> (d_normals.GetDevicePtr(), d_zLast.GetDevicePtr(), d_pixelIndex.GetDevicePtr(), h, w);
            CUDA_CHECK;
        }
        
        
        
        void cusolverLinearSystem() {
            int Lwork = 0; //, devInfo = 0;
            float* d_workspace = nullptr;
            int* d_devInfo = nullptr;
            
            cudaMalloc(&d_devInfo, sizeof(int));
            CUDA_CHECK;
            
            // get needed buffersize
            CUSOLVER_CHECK(cusolverDnSpotrf_bufferSize(handle, CUBLAS_FILL_MODE_LOWER, 4, d_A.GetDevicePtr(), 4, &Lwork));
            
            // do Cholesky
            cudaMalloc(&d_workspace, sizeof(float)*Lwork);
            CUDA_CHECK;
            CUSOLVER_CHECK(cusolverDnSpotrf(handle, CUBLAS_FILL_MODE_LOWER, 4, d_A.GetDevicePtr(), 4, d_workspace, Lwork, d_devInfo));
            //cudaMemcpy(&devInfo, d_devInfo, sizeof(int), cudaMemcpyDeviceToHost);
            //CUDA_CHECK;
            //if(devInfo != 0) std::cout << "decomp: " << devInfo << std::endl;

            // solve A*x=b
            CUSOLVER_CHECK(cusolverDnSpotrs(handle, CUBLAS_FILL_MODE_LOWER, 4, 1, d_A.GetDevicePtr(), 4, d_b.GetDevicePtr(), 4, d_devInfo));
            //cudaMemcpy(&devInfo, d_devInfo, sizeof(int), cudaMemcpyDeviceToHost);
            //CUDA_CHECK;
            //if(devInfo != 0) std::cout << "solver: " << devInfo << std::endl;
            
            cudaFree(d_workspace);
            CUDA_CHECK;
            cudaFree(d_devInfo);
            CUDA_CHECK;
        }
        
        
        
        void cuspComputeRho(float lambda) {
            // create a matrix-free linear operator
            rhoBeta A(amountValidPixels, h, w, lambda, -1.f, grid1D, block1D, d_S.GetDevicePtr(), d_PixelCoords.GetDevicePtr(), 
            d_PixelCoordsToPosHashmap.GetDevicePtr(), d_weights.GetDevicePtr());

            thrust::device_ptr<float> thrustValidIntensity(d_validIntensityPixels.GetDevicePtr());
            // allocate storage for solution (x) and right hand side (b)
            cusp::array1d<float, cusp::device_memory> d_x(A.num_rows, 0);
            cusp::array1d<float, cusp::device_memory> d_b(thrustValidIntensity, thrustValidIntensity + A.num_rows);

            // set stopping criteria:
            cusp::monitor<float> monitor(d_b, 100, 1e-1, 0, false);

            // solve the linear system A * x = b with the Conjugate Gradient method
            cusp::krylov::cg(A, d_x, d_b, monitor);
            
            // copy data to output buffer 
            copyBackKernel <<<grid1D, block1D>>> (d_rho.GetDevicePtr(), (float*)d_x.data().get(), d_PixelCoords.GetDevicePtr(), amountValidPixels, w);
            CUDA_CHECK;
        }
        
        
        
        void precomputeWeights() {
            precomputeWeightsKernel <<< grid1D, block1D>>> (d_intensity.GetDevicePtr(), d_depthOut.GetDevicePtr(), d_weights.GetDevicePtr(),
            d_weightsOne.GetDevicePtr(), d_PixelCoords.GetDevicePtr(), d_PixelCoordsToPosHashmap.GetDevicePtr(), h, w, amountValidPixels);
            CUDA_CHECK;
        }
        
        
        
        void multiplyShadingWithRhoAndSubstractFromIntensity() {
            multiplyShadingWithRhoAndSubstractFromIntensityKernel <<< grid, block >>> (d_S.GetDevicePtr(), d_rho.GetDevicePtr(), 
            d_intensity.GetDevicePtr(), d_IntensityMinusShadingTimesRho.GetDevicePtr(), h, w);
            CUDA_CHECK;
        }
        
        
        
        
        void cuspComputeBeta(float lambda1, float lambda2) {
            // create a matrix-free linear operator
            rhoBeta A(amountValidPixels, h, w, lambda1, lambda2, grid1D, block1D, d_S.GetDevicePtr(), d_PixelCoords.GetDevicePtr(), d_PixelCoordsToPosHashmap.GetDevicePtr(), d_weights.GetDevicePtr());

            thrust::device_ptr<float> thrustValidIntensity(d_validIntensityMinusShadingTimesRhoPixels.GetDevicePtr());
            // allocate storage for solution (x) and right hand side (b)
            cusp::array1d<float, cusp::device_memory> d_x(A.num_rows, 0);
            cusp::array1d<float, cusp::device_memory> d_b(thrustValidIntensity, thrustValidIntensity + A.num_rows);

            // set stopping criteria:
            cusp::monitor<float> monitor(d_b, 100, 1e-1, 0, false);

            // solve the linear system A * x = b with the Conjugate Gradient method
            cusp::krylov::cg(A, d_x, d_b, monitor);
            
            // copy data to output buffer 
            copyBackKernel <<<grid1D, block1D>>> (d_beta.GetDevicePtr(), (float*)d_x.data().get(), d_PixelCoords.GetDevicePtr(), amountValidPixels, w);
            CUDA_CHECK;
        }
        
        
        
        void computeRefinedDepth(uint32_t iterations, float lambdaZ1, float lambdaZ2) {
            d_zLast.CopyDeviceToDevice(d_depthOut.GetDevicePtr());
            
            for(uint32_t it = 0; it < iterations; it++) {
                // prepare
                createValidPixelsAndHashmapKernel <<<grid1D, block1D>>> (d_zLast.GetDevicePtr(), d_validDepthPixels.GetDevicePtr(), w, amountValidPixels,
                        d_PixelCoords.GetDevicePtr(), d_PixelCoordsToPosHashmap.GetDevicePtr());
                CUDA_CHECK;
                
                
                PrecomputeWeightsDatatermKernel <<< grid1D, block1D >>> (d_validDepthPixels.GetDevicePtr(),
                        d_weightsDataterm.GetDevicePtr(), d_PixelCoords.GetDevicePtr(), d_PixelCoordsToPosHashmap.GetDevicePtr(), h, w, amountValidPixels);
                
                precomputeBForDatatermKernel <<<grid1D, block1D >>> (d_validDepthPixels.GetDevicePtr(), d_rho.GetDevicePtr(), 
                        d_intensityMinusBeta.GetDevicePtr(), d_b.GetDevicePtr(), d_bDataterm.GetDevicePtr(), d_weightsDataterm.GetDevicePtr(), 
                        d_PixelCoords.GetDevicePtr(), d_PixelCoordsToPosHashmap.GetDevicePtr(), h, w, amountValidPixels, lambdaZ1);
            
            
                
                // solve 
                dataterm ADataterm(amountValidPixels, h, w, lambdaZ1, lambdaZ2, grid1D, block1D, d_rho.GetDevicePtr(), d_PixelCoords.GetDevicePtr(),
                        d_PixelCoordsToPosHashmap.GetDevicePtr(), d_weightsDataterm.GetDevicePtr(), d_b.GetDevicePtr(), d_weightsOne.GetDevicePtr());

                thrust::device_ptr<float> thrustbDataterm(d_bDataterm.GetDevicePtr());
                cusp::array1d<float, cusp::device_memory> d_xDataterm(ADataterm.num_rows, 0);
                cusp::array1d<float, cusp::device_memory> d_bDatatermKernel(thrustbDataterm, thrustbDataterm + ADataterm.num_rows);

                cusp::monitor<float> monitorDataterm(d_bDatatermKernel, 100, 1e-1, 0, false);

                cusp::krylov::cg(ADataterm, d_xDataterm, d_bDatatermKernel, monitorDataterm);
                
                copyBackKernel <<<grid1D, block1D>>> (d_zLast.GetDevicePtr(), (float*)d_xDataterm.data().get(), d_PixelCoords.GetDevicePtr(), amountValidPixels, w);
                CUDA_CHECK;
            }
        }
        
        
        
        void multipleAlbedoRecovery(float lambdaRho, float lambdaZ1, float lambdaZ2, uint32_t iterations, int32_t step) {
            if(step != 0)
                d_depthOut.CopyDeviceToDevice(d_zLast.GetDevicePtr());
            else
                applyBilateralFilter();
            
            createPixelIndex();
            estimateSH1Lighting();
            cusolverLinearSystem();
            visualizeSH1Lighting();
            checkNormalComputation();
            multiplyIntensityWithShading();
            createValidIntensityAndHashmap();
            precomputeWeights();
            cuspComputeRho(lambdaRho);
            multiplyShadingWithRhoAndSubstractFromIntensity();
            createValidIntensityMinusShadingTimesRho();
            cuspComputeBeta(1.f, 1.f);
            subtractBetaFromIntensity();
            computeRefinedDepth(iterations, lambdaZ1, lambdaZ2);
            checkNormalComputationRefinedDepth();
        }
        
        
        
        void download(ImageGS depthOut, ImageGS intensity, ImageRGB normals, ImageGS intenMinShTimesRho, ImageGS beta) {
            intensity.SetRawData(d_intensity.DownloadData());
            depthOut.SetRawData(d_depthOut.DownloadData());
            normals.SetRawData(d_normals.DownloadData());
            intenMinShTimesRho.SetRawData(d_IntensityMinusShadingTimesRho.DownloadData());
            beta.SetRawData(d_beta.DownloadData());
        }
        
        void downloadDepthOut(ImageGS depOut) {
        	depOut.SetRawData(d_depthOut.DownloadData());
        }
        
        void downloadAb(float** A, float** b) {
            *A = (float*)d_A.DownloadData();
            *b = (float*)d_b.DownloadData();
        }
        
        
        
        void downloadRho(ImageGS rho) {
            rho.SetRawData(d_rho.DownloadData());
        }
        
        
        
        void downloadRefinedDepth(ImageGS rD) {
            rD.SetRawData(d_zLast.DownloadData());
        }
        
        
        
        void downloadNormals(ImageRGB nrm) {
            nrm.SetRawData(d_normals.DownloadData());
        }
        
        
        
        void downloadShading(ImageGS sh) {
            sh.SetRawData(d_S.DownloadData());
        }
        
        
        
        void upload(float* m) {
            d_m.UploadData(m);
        }
};

#endif
