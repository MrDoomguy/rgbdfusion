#ifndef _COMPUTE_WEIGHT_HELPER_H_
#define _COMPUTE_WEIGHT_HELPER_H_

__device__ float checkForNaN2(float v, const char *msg)
{
  //if(v !=v) printf("nan! %s\n", msg);
  return v;
}

#define CHECK_NAN(expr) checkForNaN2((expr), #expr)

// index is centered on current weight position and not the pixel that is computed in the kernel
__device__ float computeWeightForDiagonal(RightAndLowerWeights* weights, int32_t index, int8_t deltaX, int8_t deltaY, int32_t w) {
    float weight = 0.f;
    
    // upper left
    if(deltaX == -1 && deltaY == -1) {
        RightAndLowerWeights& six = weights[index];
        RightAndLowerWeights& seven = weights[index + 1];
        RightAndLowerWeights& eleven = weights[index + w];
        
        weight = eleven.right * six.lower + six.right * seven.lower;
    }
    
    // upper right
    if(deltaX == 1 && deltaY == -1) {
        RightAndLowerWeights& eight = weights[index];
        RightAndLowerWeights& seven = weights[index - 1];
        RightAndLowerWeights& twelve = weights[index + w - 1];
        
        weight = twelve.right * eight.lower + seven.right * seven.lower;
    }
    
    // lower left
    if(deltaX == -1 && deltaY == 1) {
        RightAndLowerWeights& sixteen = weights[index];
        RightAndLowerWeights& eleven = weights[index - w];
        RightAndLowerWeights& twelve = weights[index - w + 1];
        
        weight = eleven.right * eleven.lower + sixteen.right * twelve.lower;
    }
    
    // lower right
    if(deltaX == 1 && deltaY == 1) {
        RightAndLowerWeights& twelve = weights[index - w - 1];
        RightAndLowerWeights& seventeen = weights[index - 1];
        RightAndLowerWeights& thirteen = weights[index - w];
        
        weight = twelve.right * thirteen.lower + seventeen.right * twelve.lower;
    }

    return weight;
}

__device__ float computeWeightForCenter(RightAndLowerWeights* weights, int32_t index, uint32_t w, uint32_t h, uint32_t x, uint32_t y) {
    RightAndLowerWeights zerozero;
    zerozero.right = 0.0f;
    zerozero.lower = 0.0f;
    
    RightAndLowerWeights& twelve = weights[index];
    RightAndLowerWeights eleven = zerozero;
    RightAndLowerWeights seven = zerozero;
    
    if(x > 0) eleven = weights[index - 1];
    if(y > 0) seven = weights[index - w];
    
    return (    eleven.right*eleven.right + twelve.right*twelve.right + twelve.lower*twelve.lower + seven.lower*seven.lower + (eleven.right + twelve.right + twelve.lower + seven.lower)*(eleven.right + twelve.right + twelve.lower + seven.lower)   );
}

// index is centered on current weight position and not the pixel that is computed in the kernel
__device__ float computeWeighForOuterCross(RightAndLowerWeights* weights, size_t index, int8_t deltaX, int8_t deltaY, uint32_t w) {
    float weight = 0.f;
    
    // upper upper
    if(deltaX == 0 && deltaY == -2) {
        RightAndLowerWeights& two = weights[index];
        RightAndLowerWeights& seven = weights[index + w];
        
        weight = two.lower * seven.lower;
    }
    
    // right right
    if(deltaX == 2 && deltaY == 0) {
        RightAndLowerWeights& twelve = weights[index - 2];
        RightAndLowerWeights& thirteen = weights[index - 1];
        
        weight = twelve.right * thirteen.right;
    }
    
    // lower lower
    if(deltaX == 0 && deltaY == 2) {
        RightAndLowerWeights& twelve = weights[index - w - w];
        RightAndLowerWeights& seventeen = weights[index - w];
        
        weight = twelve.lower * seventeen.lower;
    }
    
    // left left
    if(deltaX == -2 && deltaY == 0) {
        RightAndLowerWeights& ten = weights[index];
        RightAndLowerWeights& eleven = weights[index + 1];
        
        weight = ten.right * eleven.right;
    }

    return weight;
}

// index is centered on current weight position and not the pixel that is computed in the kernel
__device__ float computeWeightForInnerCross(RightAndLowerWeights* weights, int32_t index, int8_t deltaX, int8_t deltaY, uint32_t w, uint32_t h, uint32_t x, uint32_t y) {
    float weight = 0.f;
    RightAndLowerWeights zerozero;
    zerozero.right = 0.0f;
    zerozero.lower = 0.0f;
    
    // upper
    if(deltaX == 0 && deltaY == -1) {
        RightAndLowerWeights& seven = weights[index];
        RightAndLowerWeights two = zerozero;
        RightAndLowerWeights six = zerozero;
        RightAndLowerWeights eleven = zerozero;
        RightAndLowerWeights& twelve = weights[index + w];
        
        if(y > 1) two = weights[index - w];
        if(y > 0 && x > 0) six = weights[index - 1];
        if(x > 0) eleven = weights[index + w - 1];
        
        float sumSeven = two.lower + six.right + seven.lower + seven.right;
        float sumTwelve = seven.lower + eleven.right + twelve.lower + twelve.right;
        
        weight = - ( seven.lower * (sumSeven + sumTwelve) );
    }
    
    // right
    if(deltaX == 1 && deltaY == 0) {
        RightAndLowerWeights& thirteen = weights[index];
        RightAndLowerWeights seven = zerozero;
        RightAndLowerWeights eight = zerozero;
        RightAndLowerWeights eleven = zerozero;
        RightAndLowerWeights& twelve = weights[index - 1];
        
        if(y > 0) seven = weights[index - w - 1];
        if(y > 0 && x < w - 1) eight = weights[index - w];
        if(x > 0) eleven = weights[index - 2];
        
        float sumThirteen = eight.lower + twelve.right + thirteen.lower + thirteen.right;
        float sumTwelve = seven.lower + eleven.right + twelve.lower + twelve.right;
        
        weight = - ( twelve.right * (sumThirteen + sumTwelve) );
    }
    
    // lower
    if(deltaX == 0 && deltaY == 1) {
        RightAndLowerWeights seven = zerozero;
        RightAndLowerWeights& seventeen = weights[index];
        RightAndLowerWeights sixteen = zerozero;
        RightAndLowerWeights& eleven = zerozero;
        RightAndLowerWeights& twelve = weights[index - w];
        
        if(y > 0) seven = weights[index - w - w];
        if(x > 0 && y < h - 1) sixteen = weights[index  - 1];
        if(x > 0) eleven = weights[index - w - 1];
        
        float sumSeventeen = twelve.lower + sixteen.right + seventeen.lower + seventeen.right;
        float sumTwelve = seven.lower + eleven.right + twelve.lower + twelve.right;
        
        weight = - ( twelve.lower * (sumSeventeen + sumTwelve) );
    }
    
    // left
    if(deltaX == -1 && deltaY == 0) {
        RightAndLowerWeights seven = zerozero;
        RightAndLowerWeights ten = zerozero;
        RightAndLowerWeights six = zerozero;
        RightAndLowerWeights& eleven = weights[index];
        RightAndLowerWeights& twelve = weights[index + 1];
        
        if(y > 0) seven = weights[index - w + 1];
        if(x > 1) ten = weights[index - 1];
        if(x > 0 && y > 0) six = weights[index - w];
        
        float sumEleven = six.lower + ten.right + eleven.lower + eleven.right;
        float sumTwelve = seven.lower + eleven.right + twelve.lower + twelve.right;
        
        weight = - ( eleven.right * (sumEleven + sumTwelve) );
    }

    return weight;
}

#endif
